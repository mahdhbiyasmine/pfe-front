import React from 'react'
import Layout2 from './layout2'
import Sidebar2 from './sidebar2'
import { Outlet } from 'react-router-dom'
function Home2() {
  return (
    <div class="app">   	
    <Sidebar2></Sidebar2>
    <div className="app-wrapper">
      <div className="app-content pt-3 p-md-3 p-lg-4" />
      <Outlet></Outlet>
     
    </div>{/*//app-wrapper*/}    					
</div>
  
  )
}

export default Home2