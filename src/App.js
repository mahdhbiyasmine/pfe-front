import Updatesubcategory from "./Dashbord/subcategory/updatesubcategory";
import Layout from "./Dashbord/layout";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Listcategory from "./Dashbord/category/listcategory";
import AddCategory from "./Dashbord/category/addCategory";
import UpdateCategory from "./Dashbord/category/updateCategory";
import Listsubcategory from "./Dashbord/subcategory/listsubcategory";
import Addsubcategory from "./Dashbord/subcategory/addsubcategory";
import Home from "./Dashbord/home";
import Listproduct from "./Dashbord/product/listproduct";
import AddProduct from "./Dashbord/product/addProduct";
import ListServiceCategory from "./Dashbord/servicescatg/listservicecategory";
import AddServiceCategory from "./Dashbord/servicescatg/addServiceCategory";
import Updateservicecategory from "./Dashbord/servicescatg/updateservicecategory";
import Listservice from "./Dashbord/servicess/listservice";
import Addservice from "./Dashbord/servicess/addservice";
import Updateservice from "./Dashbord/servicess/updateservice";
import Listdepot from "./Dashbord/depot/listdepot";
import Adddepot from "./Dashbord/depot/adddepot";
import Updatedepot from "./Dashbord/depot/updatedepot";
import Listfournisseur from "./Dashbord/fournisseur/listfournisseur";
import Addfournisseur from "./Dashbord/fournisseur/addfournisseur";
import Updatefournisseur from "./Dashbord/fournisseur/updatefournisseur";
import Signup from "./Dashbord/login/signup";
import Account from "./Dashbord/login/account";
import Login from "./Dashbord/login/login";
import Home2 from "./dashborduser/home2";
import Layout2 from "./dashborduser/layout2";
import Index from "./user";
import Details from "./user/details";
import Service from "./user/service";
import Contact from "./user/contact";
import Devis from "./user/devis";
import QuiSommesNous from "./user/qui-sommes-nous";
import Allproduct from "./user/allproduct";
import Listdevis from "./Dashbord/devis/listdevis";
import Panier from "./user/panier";
import AccountAdmin from "./Dashbord/login/accountadmin";
import UpdateProduct from "./Dashbord/product/updateProduct";
import Favorie from "./user/favourie";
import EditProfile from "./Dashbord/login/edit";

function App() {
  
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/dashbord" element={<Home></Home>}>
        <Route path="/dashbord" element={<Layout></Layout>}></Route></Route>
          <Route  path="/dashbord/category"  element={<Listcategory></Listcategory>}></Route>
          <Route  path="/dashbord/addcategory"  element={<AddCategory></AddCategory>}></Route>{" "}
          <Route  path="/dashbord/updatecategory/:id"  element={<UpdateCategory></UpdateCategory>}></Route>
          <Route  path="/dashbord/addsubcategory"  element={<Addsubcategory></Addsubcategory>}></Route>{" "}
          <Route  path="/dashbord/subcategory"  element={<Listsubcategory></Listsubcategory>}></Route>
          <Route  path="/dashbord/updatesubcategory/:id"  element={<Updatesubcategory></Updatesubcategory>}></Route>
          <Route path="/dashbord/product"element={<Listproduct></Listproduct>}></Route>{" "}
          <Route path="/dashbord/addproduct" element={<AddProduct></AddProduct >}></Route>
          <Route path="/dashbord/listservicecategory" element={<ListServiceCategory></ListServiceCategory >}></Route>
          <Route path="/dashbord/addservicecategory" element={<AddServiceCategory></AddServiceCategory >}></Route>
          <Route path="/dashbord/updateservicecategory/:id" element={<Updateservicecategory></Updateservicecategory >}></Route>
          <Route path="/dashbord/listservice" element={<Listservice></Listservice >}></Route>
          <Route path="/dashbord/addservice" element={<Addservice></Addservice >}></Route>
          <Route path="/dashbord/updateservice/:id" element={<Updateservice></Updateservice >}></Route>
          <Route path="/dashbord/listdepot" element={<Listdepot></Listdepot >}></Route>
          <Route path="/dashbord/adddepot" element={<Adddepot></Adddepot >}></Route>
          <Route path="/dashbord/updatedepot/:id" element={<Updatedepot></Updatedepot >}></Route>
          <Route path="/dashbord/listprovider" element={<Listfournisseur></Listfournisseur >}></Route>
          <Route path="/dashbord/addprovider" element={<Addfournisseur></Addfournisseur >}></Route>
          <Route path="/dashbord/updateprovider/:id" element={<Updatefournisseur></Updatefournisseur >}></Route>
          <Route path="/dashbord/devis" element={<Listdevis></Listdevis>}></Route>
                    <Route path="/dashbord/account" element={<AccountAdmin></AccountAdmin>}></Route>
                    <Route path="/dashbord/updateproduct/:id" element={<UpdateProduct></UpdateProduct>}></Route>
                    <Route path="/dashbord/editProfile/:id" element={<EditProfile></EditProfile>}></Route>


    {/* dasboard user */}
    <Route path="/userdashboard" element={<Home2></Home2>}>
     <Route path="/userdashboard" element={<Layout2></Layout2>}></Route>
     <Route path="/userdashboard/account" element={<Account></Account>}></Route>

          </Route>

    {/* site */}
    <Route path="/" element={<Index></Index>}></Route>
    <Route path="/signup" element={<Signup></Signup>}></Route>
    <Route path="/login" element={<Login></Login>}></Route>
    <Route path="/details/:id" element={<Details></Details>}></Route>
    <Route path="/service" element={<Service></Service>}></Route>
    <Route path="/contact" element={<Contact></Contact>}></Route>
    <Route path="/devis" element={<Devis></Devis>}></Route>
    <Route path="/qui-sommes-nous" element={<QuiSommesNous></QuiSommesNous>}></Route>
    <Route path="/allproduct" element={<Allproduct></Allproduct>}></Route>
    <Route path="/allproduct" element={<Allproduct></Allproduct>}></Route>
    <Route path="/panier" element={<Panier></Panier>}></Route>
    <Route path="/favoris" element={<Favorie></Favorie>}></Route>

        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
