import React, { useEffect, useState } from 'react';
import Sidebar3 from './sidebar3';
import Footer from './footer';
import { Link } from 'react-router-dom';
import { jwtDecode } from 'jwt-decode';
import orderservice from '../services/orderservice';

function Panier() {

  const [cartItems, setCartItems] = useState([])
  console.log("cartItems", cartItems)

  useEffect(() => {
    const existCart = JSON.parse(localStorage.getItem("cart"))
    setCartItems(existCart)
  }, [])

  const removeFromCart = (index) => {
    const updatedCart = [...cartItems];
    updatedCart.splice(index, 1);
    setCartItems(updatedCart);
    localStorage.setItem("cart", JSON.stringify(updatedCart));
  }
  const calculateTotal = () => {
    return cartItems.reduce((total, item) => total + item.total, 0);
  }
  //date d'aujourdhui:
  var d=new Date();
  var date=d.getFullYear()+" - "+(d.getMonth()+1)+" - "+d.getDate();
  console.log(date);
  //token :id
  const token = localStorage.getItem("token");
  const decoded = jwtDecode(token);
  const userId = decoded.sub;
  console.log(userId)
  //object :

  const [data, setData] = useState({});
  //const [cmdLine, setcmdLine] = useState({});

  const onChangeHandler = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
};
    const cmd = {
    status: "encours",
    orderDate: date,
    totalAmount: calculateTotal(),
    modelivraision: data.modelivraision,
    modepaiement: data.modepaiement,
    governorate:data.governorate,
    city: data.city,
    address: data.address,
    postalCode: data.postalCode,
    phone: data.phone,
    UserId: userId,
                    
  }
  console.log("command",cmd)
  const onSubmitHandler = (e) => {
  e.preventDefault();
orderservice.create(cmd)          
.then((res) => {
  console.log(res);
})
.catch((erreur) => {
  console.log(erreur);
});
};

  const cmdLine = {
    quantity:cartItems.quantity, 
       ProductId:cartItems.id,
       //OrderId:res.id,

  }
  console.log("cmdline",cmdLine)
//   const onChangeHandler = (e) => {
//     setCmd({ ...cmd, [e.target.name]: e.target.value });
//     console.log(cmd);
// };


  return (
    <div>
      <Sidebar3 />
      <div className="col-lg-12">
                <div className="main_title">
                  <h2><span style={{ fontSize: "40px", fontFamily: "VotrePoliceSpeciale", color: "green", }}>Panier</span></h2>
              
              <div className="banner_content d-md-flex justify-content-between align-items-center">
              <table className="table" >
                <thead>
                  <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Total</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {cartItems.map((item, index) => (
                    <tr key={index}>
                      <Link   to ={`/details/${item.id}`}style={{ color: 'green', textDecoration: 'none' }}>
                      <td >{item.titre}</td></Link>
                      <td>{item.price}</td>
                      <td>{item.quantity}</td>
                      <td>{item.total}</td>
                      <td><button style={{width:"30px",height:"30px"}} className="btn btn-danger btn-rounded btn-sm cursor-pointer" onClick={() => removeFromCart(index)}><span className="fa fa-times" ></span></button></td>
                    </tr>
                  ))}
                   <tr>
                    <td colSpan="3"  scope="col"style={{ textAlign: "right" }}>Total :</td>
                    <td>{calculateTotal()} DTN</td>
                    <td></td>
                  </tr>
                </tbody>
              </table>
              </div>  </div>
              </div>
    
   

      <div className="section-top-border" style={{ marginLeft: "5%" }}>
        <div className="row">
          <div className="col-lg-8 col-md-8">
            <h3 className="mb-30 title_color">Adresse</h3>
            <form action="#">
              <div className="mt-10">
                <input type="text" name="governorate" placeholder="gouvernorat" required className="single-input" onChange={onChangeHandler} />
              </div>
              <div className="mt-10">
                <input type="text" name="city" placeholder="ville" required className="single-input" onChange={onChangeHandler}/>
              </div>
              <div className="mt-10">
                <input type="text" name="address" placeholder="adresse" required className="single-input" onChange={onChangeHandler}/>
              </div>
              <div className="mt-10">
                <input type="number" name="postalCode" placeholder="Code postal" required className="single-input" onChange={onChangeHandler}/>
              </div>
              <div className="mt-10">
                <input type="text" name="phone" placeholder="phone" required className="single-input" onChange={onChangeHandler}/>
              </div>
             
            </form>
          </div>
          <div className="col-lg-3 col-md-4 mt-sm-30 element-wrap">
          <div className="single-element-widget">
  <h3 className="mb-30 title_color">Méthode de livraison :</h3>
  <form>
    <div className="switch-wrap d-flex justify-content-between align-items-center">
      <select id="delivery-select" name="modelivraision" className="form-control" onChange={onChangeHandler}>
        <option value="A domicile" selected>A domicile</option>
        <option value="Ramassage en magasin">Ramassage en magasin</option>
      </select>
    </div>
  </form>
</div>

<div className="single-element-widget">
  <h3 className="mb-30 title_color">Méthode de paiement :</h3>
  <form>
    <div className="switch-wrap d-flex justify-content-between align-items-center">
      <select id="payment-select"   name="modepaiement"className="form-control" onChange={onChangeHandler}>
        <option value="Carte bancaire" selected>Carte bancaire</option>
        <option value="chèque">Chèque</option>
        <option value="Espèce">Espèces</option>
      </select>
    </div>
    <button
    type="submit"
    className="main_btn"
    style={{ marginTop: '5%', marginLeft: '40%'   , height: '50px', width: '200px' }}
  onSubmit={onSubmitHandler}
  >
    Achetez 
  </button>
  </form>
  
</div>


          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Panier;
  