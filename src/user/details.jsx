import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import productservice from "../services/productservice";
import { Button } from "react-bootstrap";
import Footer from "./footer";
import Sidebar3 from "./sidebar3";

function Details() {
  const [data, setData] = useState({});
  const { id } = useParams();
  const [affiche, setAffiche] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [addToCartDisabled, setAddToCartDisabled] = useState(false);

  useEffect(() => {
    productservice
      .GetOne(id)
      .then((res) => {
        setData(res.data);
        setAffiche(true);
      })
      .catch((erreur) => {
        console.log(erreur);
      });
  }, [id]);

  useEffect(() => {
    if (quantity > data.stock) {
      setAddToCartDisabled(true);
    } else {
      setAddToCartDisabled(false);
    }
  }, [quantity, data.stock]);

  const isConnected = localStorage.getItem("token");

  const total = quantity * data.price;

  const addItem = () => {
    const cartItems = {
      id: data.id,
      titre: data.reference,
      price: data.price,
      quantity: quantity,
      total: total,
    };

    let existCart = JSON.parse(localStorage.getItem("cart"));

    if (!existCart || !Array.isArray(existCart)) {
      existCart = [];
    }

    const newItem = [...existCart, cartItems];

    localStorage.setItem("cart", JSON.stringify(newItem));
  };

    const stockClass = data.stock > 1 ? "text-success" : "text-danger";

    return (
      <div>
        <Sidebar3 ></Sidebar3>

        <div>
          <div className="product_image_area">
            <div className="container">
              <div className="row s_product_inner">
                <div className="col-lg-6">
                  <div className="s_product_img">
                    <div
                      id="carouselExampleIndicators"
                      className="carousel slide"
                      data-ride="carousel"
                    >
                      <img
                        style={{ width: "250px", height: "250px" }}
                        src={
                          "http://localhost:4000/file/product/" + data?.image
                        }
                        alt={`Product ${data?.id}`}
                      />
                    </div>
                  </div>
                </div>
                <div className="col-lg-5 offset-lg-1">
                  <div className="s_product_text">
                    <h3>{data?.reference}</h3>
                    <h2>{data?.price} DTN</h2>
                    <ul className="list">
                      {/* <li>
                        <a className="active" href="#">
                          <span>SubCategory</span> : {data?.SubCategoryId}
                        </a>
                      </li> */}
                      <li>
                        <span className={stockClass}>
                          {data?.stock > 0 ? "Disponible" : "Non disponible"}
                        </span>
                      </li>
                    </ul>
                    <p>{data?.description}</p>
                    <div className="product_count">
                      <label htmlFor="qty">Quantité:</label>
                      <input
                        type="number"
                        name="quantity"
                        id="quantity"
                        maxLength={12}
                        value={quantity}
                        onChange={(e) =>
                          setQuantity(parseInt(e.target.value))
                        }
                        className="input-text qty"
                      />
                      {quantity > data.stock && (
                        <p style={{ color: "red" }}>
                          Quantité non disponible en stock
                        </p>
                      )}
                    </div>
                    <div className="card_area">
                      {isConnected ? (
                        <Button
                          className="main_btn"
                          onClick={addItem}
                          disabled={addToCartDisabled}
                        >
                          Ajouter au panier
                        </Button>
                        
                      ) : (
                        <Button className="main_btn" to="/login">
                          Ajouter au panier
                        </Button>
                      )}
                    <i className="ti-heart" 
                  style={{ marginBottom: "45%", fontSize: "25px" ,color:"green"}}
                      >
                        
                      </i>
                  </div>                    </div>

                </div>
              </div>
            </div>
          </div>
          <section className="product_description_area">
            <div className="container"></div>
          </section>
          <Footer />
        </div>
      </div>
    );
  }

export default Details;
