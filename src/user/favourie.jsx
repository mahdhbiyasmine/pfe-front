import React from "react";
import Sidebar3 from "./sidebar3";
import Footer from "./footer";

function Favorie() {
  return (
    <div>
      <Sidebar3></Sidebar3>

      <div className="section-top-border">
        <h3
          className="mb-30 title_color"
          style={{ marginLeft: "10%", color: "green" }}
        >
          Favoris
        </h3>
        <div className="progress-table-wrap">
          <div className="progress-table">
            <div className="table-head">
              <div className="serial"></div>
              <div className="country">Produit</div>
              <div className="visit">Prix</div>
              <div className="visit">Supprimer</div>
            </div>
            <div className="table-row">
              <div className="serial"></div>
              <div className="visit"></div>
              <div className="percentage"></div>
            </div>
          </div>
        </div>
      </div>

      <Footer></Footer>
    </div>
  );
}

export default Favorie;
