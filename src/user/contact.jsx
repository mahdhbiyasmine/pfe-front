import React, { useRef } from 'react';
import emailjs from "emailjs-com";
import Sidebar3 from './sidebar3'
import Footer from './footer'

function Contact() {
    
    const form=useRef()
    const sendEmail=(e)=>{
        e.preventDefault()
        emailjs.sendForm("service_gcu2izv","template_2du6zg9",form.current,"s-Ubdm1DbmyKhQj9K")
        .then((res)=>{
            alert("message envoyer !");
            console.log(res);

        }).catch((error)=>{
            console.log(error);
        })

        
        
    }

  return (
   <div>
    <Sidebar3></Sidebar3>
 
  {/*================End Home Banner Area =================*/}
  {/* ================ contact section start ================= */}
  <section className="section_gap">
    <div className="container">
     
      <div className="row">
        <div className="col-12">
          <h2 className="contact-title">Contact </h2>
        </div>
        <div className="col-lg-8 mb-4 mb-lg-0">
          <form className="form-contact contact_form" action="" method="post" id="contactForm" noValidate="novalidate" ref={form} onSubmit={sendEmail}>
            <div className="row">
            
              <div className="col-sm-6">
                <div className="form-group">
                  <input className="form-control" name="from_name" id="name" type="text" placeholder="Votre nom" />
                </div>
              </div>
              <div className="col-sm-6">
                <div className="form-group">
                  <input className="form-control"  name="to_name" id="email" type="email" placeholder="votre email" />
                </div>
              </div>
              <div className="col-12">
                <div className="form-group">
                  <input className="form-control" name="subject" id="subject" type="text" placeholder="Votre Suijet" />
                </div>
              </div>

            </div>
            <div className="col-12">
                <div className="form-group">
                  <textarea className="form-control w-100" name="message" id="message" cols={30} rows={9} placeholder="Votre Message" defaultValue={""} />
                </div>
              </div>
            <div className="form-group mt-lg-3">
              <button type="submit" className="main_btn">Envoyer Message</button>
            </div>
          </form>
        </div>
        <div className="col-lg-4">
          <div className="media contact-info">
            <span className="contact-info__icon"><i className="ti-home" /></span>
            <div className="media-body">
              <h3>Tunis, Sousse.</h3>
            </div>
          </div>
          <div className="media contact-info">
            <span className="contact-info__icon"><i className="ti-tablet" /></span>
            <div className="media-body">
              <h3><a href="tel:454545654">+216 72 225 336</a></h3>
              <p>Lun à Ven 9h à 18h</p>
            </div>
          </div>
          <div className="media contact-info">
            <span className="contact-info__icon"><i className="ti-email" /></span>
            <div className="media-body">
              <h3><a href="mailto:support@colorlib.com">Jardin.pipéniere@gmail.com</a></h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <Footer></Footer>
</div>


  )
}


export default Contact
