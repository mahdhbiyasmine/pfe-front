import Sidebar3 from './sidebar3'
import Footer from './footer'
import React, { useState } from 'react'
import { useEffect } from 'react'
import productservice from '../services/productservice'
import { Link } from 'react-router-dom'
import categoryservice from '../services/categoryservice'

function Allproduct() {
  const inputStyle = {
    backgroundColor: '#f4f4f4',
    color: '#333',
    border: 'none',
    borderRadius: '25px',
    padding: '12px 20px 12px 40px', // Ajustez le padding pour laisser de l'espace à l'icône
    width: '40%', // Ajustez la largeur selon vos besoins
    marginLeft: '800px', // Centrer l'élément horizontalement
    marginRight: 'auto', // Centrer l'élément horizontalement
    marginTop: '10px', // Ajout de marge en haut
    marginBottom: '10px', // Ajout de marge en bas
    fontSize: '15px', // Taille de la police
    boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.1)',
    outline: 'none',
  };
  
  const iconStyle = {
    position: 'absolute',
    top: '50%',
    left: '1200px',
    transform: 'translateY(-50%)',
    cursor: 'pointer',
  };
  
  
    const [product,setProduct]=useState([])

    const [affiche,setAffiche]=useState(false)
    const getAllProduct=()=>{
      productservice.GetAll().then((res)=>{
      
    
        setProduct(res.data);
        setAffiche(true)
      }).catch((erreur)=>{console.log()})
    }
    useEffect(()=>{
      getAllProduct()
    },[])
   
    const [inputText, setInputText] = useState("");
    let inputHandler = (e) => {
      //convert input text to lower case
      var lowerCase = e.target.value.toLowerCase();
      setInputText(lowerCase);
    };
    const filteredData = product?.filter((el) => {
      if (inputText === '') {
          return el;
      } else {
          return el.reference.toLowerCase().includes(inputText)
      }
    })
    if (affiche)
    {
  return (
    <div><Sidebar3></Sidebar3>
    {/* <section className="logo" style={{ position: "relative" }}>
        <img style={{ width: "100%", height:"450px"}} src="img/m.jpg" alt="" />
        <div className="text-overlay" style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}>
          <h2 style={{ color: "white", fontSize: "36px", fontFamily: "Arial, sans-serif", textAlign: "left", textShadow: "2px 2px 4px rgba(0, 0, 0, 0.3)", transition: "color 0.3s" }}>Découvrez notre collection</h2>
          <p style={{ color: "white", fontSize: "18px", fontFamily: "Arial, sans-serif", textAlign: "left", marginTop: "20px", transition: "color 0.3s" }}>Explorez notre sélection de plantes et de fleurs pour créer un jardin magnifique et vivant.</p>
        </div>
      </section> */}
  <section className="feature_product_area section_gap_bottom_custom">
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-lg-12">
        <div className="main_title" style={{marginTop:"0%",marginBottom:"10%"}}>
 <h2><span>Tout les produits</span></h2>
            <p></p>

            <form className="table-search-form row gx-1 align-items-center">
      <div className="col-auto position-relative">
        <input
          type="text"
          id="search-orders"
          name="search"
          className="form-control search-orders"
          placeholder="Rechercher"
          onChange={inputHandler}
          style={inputStyle}
        />
        {/* Icône de recherche SVG */}
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          width="24"
          height="24"
          fill="#333"
          style={iconStyle}
        >
          <path d="M0 0h24v24H0z" fill="none" />
          <path d="M15.5 14h-.79l-.28-.27a6.5 6.5 0 0 0 1.48-5.34c-.47-2.78-2.79-5-5.59-5.34a6.505 6.505 0 0 0-7.27 7.27c.34 2.8 2.56 5.12 5.34 5.59a6.5 6.5 0 0 0 5.34-1.48l.27.28v.79l4.25 4.25c.41.41 1.08.41 1.49 0 .41-.41.41-1.08 0-1.49L15.5 14zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" />
        </svg>
      </div>
    </form>
                     </div>
        </div>
      </div>
      
      <div className="row">
      {/* <div class="col-lg-3">
            <div class="left_sidebar_area">
              <aside class="left_widgets p_filter_widgets">
                <div class="l_w_title">
                  <h3> Categories</h3>
                </div>
                <div class="widgets_inner">
                  <ul class="list">
                    
                    <li>
                      
                      <a href="#">Frozen Fish</a>
                    </li>
                    
                  </ul>
                </div>
              </aside>

            

            </div>
          </div> */}
      {filteredData?.map((item)=>{
                        return(
        <div className="col-lg-3 col-md-6">
          <div className="single-product">
            <div className="product-img">
            <img
                         style={{ width: '250px', height: '250px' }}
                         src={'http://localhost:4000/file/product/' + item.image}
                         alt={`Product ${item.id}`}
                       />            {/* {item.image.map((i) => {
                        return (
                            <img style={{ width: '80px', height: '80px' }} src={'http://localhost:4000/file/product/' + i} />
                        )
                    })}    */}
                        <div className="p_icon">
                  <Link to={`/details/${item.id}`}>
                        <a className="option1"><i className="ti-eye"/></a>
                        </Link>
                <a href="#">
                  <i className="ti-heart" />
                </a>
                <a href="#">
                  <i className="ti-shopping-cart" />
                </a>
              </div>
            </div>
            <div className="product-btm">
              <a href="#" className="d-block">
                <h4>{item.reference}</h4>
              </a>
              <div className="mt-3">
                <span className="mr-4">{item.price} DTN</span>
                <del></del>
              </div>
            </div>
          </div>
        </div>
       )
      })}
      </div>
    </div>
  </section>
    
    <Footer></Footer></div>
  )
}}

export default Allproduct