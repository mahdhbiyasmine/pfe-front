import React from 'react'
import Sidebar3 from './sidebar3'
import Footer from './footer'
import { Link } from 'react-router-dom'

function QuiSommesNous() {
  return (
  <div>
    <Sidebar3></Sidebar3>
    <section className="logo" style={{ position: "relative" }}>
        <img style={{ width: "100%", height:"350px"}} src="img/image10.jpg" alt="" />
        <div className="text-overlay" style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}>
          <h2 style={{ color: "white", fontSize: "36px", fontFamily: "Arial, sans-serif", textAlign: "left", textShadow: "2px 2px 4px rgba(0, 0, 0, 0.3)", transition: "color 0.3s" }}>A votre service !</h2>
          <p style={{ color: "white", fontSize: "18px", fontFamily: "Arial, sans-serif", textAlign: "left", marginTop: "20px", transition: "color 0.3s" }}>Explorez notre sélection de plantes et de fleurs pour créer un jardin magnifique et vivant.</p>
        </div>
      </section>
      
    <div className="whole-wrap">
    <div className="container">
    

      <div className="section-top-border">
        <h3 className="mb-30 title_color">           Qui Somme Nous ?
</h3>
        <div className="row">
          <div className="col-lg-12">
            <blockquote className="generic-blockquote">
            GreenGarden Tunisie, établie en 2024, se distingue en tant qu'entreprise polyvalente proposant une gamme complète de produits agricoles de qualité supérieure, ainsi que des services dédiés à l'aménagement et l'entretien de jardins et terrasses. Spécialisée dans la fourniture de solutions innovantes et durables, GreenGarden Tunisie s'engage à satisfaire les besoins de ses clients tout en préservant l'environnement.

En tant que leader du marché, GreenGarden Tunisie se positionne comme un partenaire de confiance pour les agriculteurs, les passionnés de jardinage et les propriétaires de terrasses. Nos produits de pointe, soigneusement sélectionnés pour leur efficacité et leur respect de l'écosystème, sont accompagnés d'un service clientèle de premier ordre. Nous mettons un point d'honneur à comprendre les besoins individuels de chaque client et à leur fournir des solutions sur mesure qui répondent à leurs exigences spécifiques.

Fondée sur les principes de l'innovation, de la durabilité et de l'excellence, GreenGarden Tunisie s'efforce constamment d'élever les normes de l'industrie agricole et de l'aménagement paysager. Notre équipe dévouée travaille avec passion pour offrir à nos clients des produits et des services de la plus haute qualité, tout en contribuant à promouvoir un mode de vie plus vert et plus harmonieux.

Que ce soit pour améliorer la productivité de vos cultures, créer un espace extérieur de rêve ou simplement pour profiter de la beauté de la nature, GreenGarden Tunisie est là pour vous accompagner à chaque étape de votre projet. Rejoignez-nous dans notre mission pour un avenir plus vert et plus florissant.
            </blockquote>
          </div>
        </div>
      </div>
      <Link to="/contact" class="main_btn mt-40 ">Contact
					<span class="lnr lnr-arrow-right"></span>
				</Link>
    </div>
  </div>
  <div className="whole-wrap">
    <div className="container">
    

      <div className="section-top-border">
        <h3 className="mb-30 title_color">            UNE ÉQUIPE À VOTRE SERVICE !
</h3>
        <div className="row">
          <div className="col-lg-12">
            <blockquote className="generic-blockquote">
Le jardin c’est vous, les papiers c’est nous ! Toute notre équipe est à votre entière disponibilité du lundi au vendredi par téléphone et par mail. Nous mettons un point d’honneur à être facilement joignable pour répondre rapidement à vos demandes.

            </blockquote>
          </div>
        </div>
      </div>

    </div>
  </div>

  <Footer></Footer>
</div>

  )
}

export default QuiSommesNous