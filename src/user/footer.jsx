import React from 'react';

function Footer() {
  return (
    <footer className="footer">
      <div className="container">
        <div className="media contact-info">
          <span className="contact-info__icon"><i className="ti-home" /></span>
          <div className="media-body">
            <h3>Tunis, Sousse.</h3>
          </div>
        </div>
        <div className="media contact-info">
          <span className="contact-info__icon"><i className="ti-tablet" /></span>
          <div className="media-body">
            <h3><a href="tel:454545654">+216 72 225 336</a></h3>
            <p>Lun à Ven 9h à 18h</p>
          </div>
        </div>
        
        <div className="media contact-info">
          <span className="contact-info__icon"><i className="ti-email" /></span>
          <div className="media-body">
            <h3><a href="mailto:support@colorlib.com">Jardin.pipéniere@gmail.com</a></h3>
          </div>
        </div>
      </div>
      <style>
        {`
          .footer {
            background-color: #EFFCEF;
            padding: 50px 0;
          }
          
          .footer .container {
            display: flex;
            justify-content: space-between;
          }
          
          .footer .contact-info {
            margin-bottom: 15px;
          }
          
          .footer .contact-info h3 {
            font-size: 14px;
            margin-bottom: 10px;
          }
          
          .footer .contact-info p {
            font-size: 14px;
            color: #777;
          }
          
          .footer .contact-info a {
            color: #333;
            text-decoration: none;
          }
          
          .footer .contact-info a:hover {
            text-decoration: underline;
          }
          
          .footer .contact-info__icon {
            margin-right: 15px;
          }
          
          .footer .contact-info__icon i {
            font-size: 20px;
            color: #333;
          }
        `}
      </style>
    </footer>
  );
}

export default Footer;
