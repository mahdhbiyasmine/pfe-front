import { jwtDecode } from "jwt-decode";
import React from "react";
import { Link } from "react-router-dom";

function Sidebar3() {
  const isConnected = localStorage.getItem("token");
  let role = "";

  if (isConnected) {
    const decoded = jwtDecode(isConnected);
    role = decoded.role;
  }

  return (
    <div>
      <header className="header_area">
        <nav className="navbar navbar-expand-lg navbar-light w-100">
          <Link
            className="navbar-brand logo_h"
            to="/"
            style={{ textDecoration: "none" }}
          >
            <img
              style={{
                color: "#41F02D",
                fontWeight: "bold",
                marginLeft: "50%",
                width: "90px",
                height: "90px",
              }}
              src="img/logo.jpeg"
              alt=""
            />
          </Link>

          <div
            className="collapse navbar-collapse offset w-100"
            id="navbarSupportedContent"
          >
            <div className="row w-100 mr-0">
              <div className="col-lg-7 pr-0">
                <ul className="nav navbar-nav center_nav pull-right">
                  <li className="nav-item">
                    <Link className="nav-link" to="/">
                      Accueil
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/allproduct">
                      Produit
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/service">
                      Service
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/devis">
                      Devis
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/qui-sommes-nous">
                      A propos
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/contact">
                      Contact
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="col-lg-5 pr-0">
                <ul className="nav navbar-nav navbar-right right_nav pull-right">
                  <li className="nav-item">
                    {/* Condition based on role */}
                    {role === "admin" ? (
                      <Link className="nav-link" to="/dashbord" class="icons">
                        <i
                          className="ti-user"
                          aria-hidden="true"
                          style={{ marginRight: "50%", fontSize: "20px" }}
                        ></i>
                      </Link>
                    ) : role === "user" ? (
                      <Link className="nav-link" to="/userdashboard" class="icons">
                        <i
                          className="ti-user"
                          aria-hidden="true"
                          style={{ marginRight: "50%", fontSize: "20px" }}
                        ></i>
                      </Link>
                    ) : (
                      <Link className="nav-link" to="/login" class="icons">
                        <i
                          className="ti-user"
                          aria-hidden="true"
                          style={{ marginRight: "50%", fontSize: "20px" }}
                        ></i>
                      </Link>
                    )}  
                  </li>
                  <li className="nav-item">
                    <Link to="/panier" class="icons">
                      <i
                        className="ti-shopping-cart"
                        style={{ marginLeft: "50%", fontSize: "25px" }}
                      ></i>
                    </Link>
                  </li>
                 
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </header>
    </div>
  );
}

export default Sidebar3;
