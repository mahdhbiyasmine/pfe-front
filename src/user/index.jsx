import React from 'react'
import Sidebar3 from './sidebar3'
import Layout3 from './layout3'
import Footer from './footer'

function Index() {
  return (
    <div>
      <Sidebar3></Sidebar3>
      <Layout3></Layout3>
      <Footer></Footer>
    </div>
  )
}

export default Index