import React, { useState } from 'react'
import { useEffect } from 'react'
import productservice from '../services/productservice'
import { Link } from 'react-router-dom'

function Layout3() {
  const [product,setProduct]=useState([])
const [affiche,setAffiche]=useState(false)
const getAllProduct=()=>{
  productservice.GetAll().then((res)=>{
  

    setProduct(res.data);
    setAffiche(true)
  }).catch((erreur)=>{console.log()})
}
useEffect(()=>{
  getAllProduct()
},[])

if (affiche)
{
  return (
<div>  <section className="logo" style={{ position: "relative" }}>
        <img style={{ width: "100%", height:"350px"}} src="img/11.jpeg" alt="" />
        <div className="text-overlay" style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}>
          <h2 style={{ color: "white", fontSize: "36px", fontFamily: "Arial, sans-serif", textAlign: "left", textShadow: "2px 2px 4px rgba(0, 0, 0, 0.3)", transition: "color 0.3s" }}>Découvrez notre collection</h2>
          <p style={{ color: "white", fontSize: "18px", fontFamily: "Arial, sans-serif", textAlign: "left", marginTop: "20px", transition: "color 0.3s" }}>Explorez notre sélection de plantes et de fleurs pour créer un jardin magnifique et vivant.</p>
        </div>
      </section>
  {/*================End Home Banner Area =================*/}
  {/* Start feature Area */}

  {/* End feature Area */}
  {/*================ Feature Product Area =================*/}

  {/*================ End Feature Product Area =================*/}
  <section className="blog-area section-gap">
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-lg-12">
          <div className="main_title" style={{marginTop:"10%",marginBottom:"10%"}}>
          <h2 style={{ fontSize: "40px", fontFamily: 'Courier New', color: "#000000" }}>
  <span style={{ fontSize: "40px" ,fontFamily: 'Open Sans, sans-serif',}}>Bienvenue chez </span>
  <span style={{ fontSize: "40px", color: "green", fontFamily: "VotrePoliceSpeciale" }}>&nbsp; Angules Jardin &nbsp;</span>
  <span style={{ fontSize: "40px" ,fontFamily: 'Open Sans, sans-serif', }}> Services</span>
</h2>            <p style={{fontSize:"20px", color: "green",}}>
            Pépinière, conception et aménagement des espaces verts, jardinerie, vente de plantes et fleurs pour jardin. Des milliers de végétaux , arbres, paysagiste, dracéna, fleurs, mobilier urbain.</p>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-4 col-md-6">
          <div className="single-blog">
            <div className="thumb">
              <img
                                       style={{ width: '350px', height: '350px' }}

              className="img-fluid" src="img/concept.jpg" alt />
            </div>
            <div className="short_details">
            
                <h4>Bureau d’études</h4>
              <div className="text-wrap">
                <p>
                Le bureau d’études de la société CAP VERT réalise l’étude de votre projet d’aménagement de parc et jardin. A l’aide d’un logiciel de modélisation 3D spécifique au                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6">
          <div className="single-blog">
            <div className="thumb">
              <img className="img-fluid" 
                                                     style={{ width: '350px', height: '350px' }}
                                                     src="img/3d.jpg" alt />
            </div>
            <div className="short_details">
             
                <h4>                Conception de parcs et jardins en 3d
</h4>
              <div className="text-wrap">
                <p>
Chaque projet d’aménagement paysager est conçu en 3D pour vous offrir une vision unique à 360° en totale immersion. Projetez-vous dans votre futur jardin grâce   </p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-md-6">
          <div className="single-blog">
            <div className="thumb">
            <img className="img-fluid" 
                                                     style={{ width: '350px', height: '350px' }}
                                                     src="img/vert.jpg" alt />            </div>
            <div className="short_details">
             
                <h4>                Aménagement des espaces verts
</h4>
              <div className="text-wrap">
                <p>
                Aménagement des espaces verts
Avec passion et enthousiasme, les paysagistes de la société Cap Vert, imaginent des parcs, des espaces verts et des jardins dans la tunisie    </p>
              </div>
              <Link to="/service" class="main_btn mt-40 ">nos service
					<span class="lnr lnr-arrow-right"></span>
				</Link>            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  {/*================ Offer Area =================*/}
  <section className="logo" style={{ position: "relative" }}>
        <img style={{ width: "100%", height:"450px"}} src="img/m.jpg" alt="" />
        <div className="text-overlay" style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}>
          <h2 style={{ color: "white", fontSize: "36px", fontFamily: "Arial, sans-serif", textAlign: "center", textShadow: "2px 2px 4px rgba(0, 0, 0, 0.3)", transition: "color 0.3s" }}>Découvrez notre produits</h2>
          <p style={{ color: "white", fontSize: "18px", fontFamily: "Arial, sans-serif", textAlign: "center", marginTop: "20px", transition: "color 0.3s" }}> Explorez notre sélection de plantes et de fleurs et de matériaux jardinés&nbsp;pour créer un jardin magnifique et vivant
.</p>
        </div>
      </section>
      <section className="feature_product_area section_gap_bottom_custom">
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-lg-12">
          <div className="main_title" style={{marginTop:"10%",marginBottom:"10%"}}>
            <h2><span>Touts les produits </span></h2>
            <p></p>
          </div>
        </div>
      </div>
      <div className="row">
      {product?.map((item)=>{
                        return(
        <div className="col-lg-4 col-md-6">
          <div className="single-product">
            <div className="product-img">
            <img
                         style={{ width: '250px', height: '250px' }}
                         src={'http://localhost:4000/file/product/' + item.image}
                         alt={`Product ${item.id}`}
                       />            {/* {item.image.map((i) => {
                        return (
                            <img style={{ width: '80px', height: '80px' }} src={'http://localhost:4000/file/product/' + i} />
                        )
                    })}    */}
                        <div className="p_icon">
                  {/* <Link to={`/details/${item.id}`}>
                        <a className="option1"><i className="ti-eye"/></a>
                        </Link>
                
                <a href="#">
                  <i className="ti-shopping-cart" />
                </a> */}
              </div>
            </div>
            <div className="product-btm">
            <Link to={`/details/${item.id}`} className="d-block">
                <h4>{item.reference}</h4>
              </Link> 
              <div className="mt-3">
                <span className="mr-4">{item.price} DTN</span>
                <del></del>
              </div>
            </div>
          </div>
        </div>
       )
      })}
      </div>
    </div>
  </section>
  <section className="feature-area section_gap_bottom_custom">
    <div className="container">
      <div className="row">
        <div className="col-lg-3 col-md-6">
          <div className="single-feature">
            <a href="#" className="title">
              <i className="flaticon-money" />
              <h3>Money back gurantee</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>
        <div className="col-lg-3 col-md-6">
          <div className="single-feature">
            <a href="#" className="title">
              <i className="flaticon-truck" />
              <h3>Free Delivery</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>
        <div className="col-lg-3 col-md-6">
          <div className="single-feature">
            <a href="#" className="title">
              <i className="flaticon-support" />
              <h3>Alway support</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>
        <div className="col-lg-3 col-md-6">
          <div className="single-feature">
            <a href="#" className="title">
              <i className="flaticon-blockchain" />
              <h3>Secure payment</h3>
            </a>
            <p>Shall open divide a one</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  {/*================ End Blog Area =================*/}
  {/*================ start footer Area  =================*/}
 </div>

  )
}
}
export default Layout3