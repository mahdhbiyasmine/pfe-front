import React, { useEffect, useState } from 'react'
import Sidebar3 from './sidebar3'
import Footer from './footer'
import serviceservice from '../services/serviceservice'

function Service() {
    const [service,setservice]=useState([])
    const [affiche,setAffiche]=useState(false)
    const getAllservice=()=>{
      serviceservice.GetAll().then((res)=>{
        console.log(res)
        setservice(res.data.data);
        setAffiche(true)
      }).catch((erreur)=>{console.log()})
    }
    useEffect(()=>{
      getAllservice()
    },[])
  return (
    <div>

        <Sidebar3></Sidebar3>
        <section className="logo" style={{ position: "relative" }}>
        <img style={{ width: "100%", height:"350px"}} src="img/11.jpeg" alt="" />
        <div className="text-overlay" style={{ position: "absolute", top: "50%", left: "50%", transform: "translate(-50%, -50%)" }}>
          <h2 style={{ color: "white", fontSize: "36px", fontFamily: "Arial, sans-serif", textAlign: "left", textShadow: "2px 2px 4px rgba(0, 0, 0, 0.3)", transition: "color 0.3s" }}>Découvrez Nos service !</h2>
          <p style={{ color: "white", fontSize: "18px", fontFamily: "Arial, sans-serif", textAlign: "left", marginTop: "20px", transition: "color 0.3s" }}></p>
        </div>
      </section>
   
    <div className="section-top-border">
    
        <div className="banner_inner d-flex align-items-center"style={{marginTop:"5%",marginBottom:"5%"}} >
      <div className="container">
      <div className="banner_content d-md-flex justify-content-between align-items-center">
          <div className="mb-3 mb-md-0">
            <h2>Service Details :</h2>
          </div>
        
        </div>
     

      </div>
    </div>
        <div class="whole-wrap">
		<div class="container">
        {service?.map((item)=>{
                              return(
    		<div class="section-top-border">
				<h3 class="mb-30 title_color">{item.name}</h3>
				<div class="row">
					<div class="col-lg-12">
						<blockquote class="generic-blockquote"style={{fontSize:"20px"}}>
							{item.description}
						</blockquote>
					</div>
          <div className="mt-2" style={{marginLeft:"70%",fontSize:"20px" ,color:"green"}}>
                <span className="mr-4">{item.cost} DTN</span>
                <del></del>
              </div>
				</div>
			</div>)})}</div></div>
            <Footer></Footer>
    </div></div>
  )
}

export default Service