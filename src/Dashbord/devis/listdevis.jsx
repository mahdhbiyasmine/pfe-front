import React, { useEffect, useState } from 'react'
import Sidebar from '../sidebar'
import devisservice from '../../services/devisservice'
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom'
function Listdevis() {



    
    const [devis,setDevis]=useState([])
    const [affiche,setAffiche]=useState(false)
    const getAllDevis=()=>{
      devisservice.GetAll().then((res)=>{
        console.log(res)
        setDevis(res.data.data);
        setAffiche(true)
      }).catch((erreur)=>{console.log()})
    }
    useEffect(()=>{
      getAllDevis()
    },[])
    //Delete {$id}
    const supprimer=(id)=>{
      Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
      }).then((result) => {
        if (result.isConfirmed) {
          devisservice.remove(id).then((res)=>{
            console.log(res)
            getAllDevis() // supression sans refresh
          }).catch((error)=>{
            console.log(error)
          })
          Swal.fire({
            title: "Deleted!",
            text: "Your file has been deleted.",
            icon: "success"
          });
        }
      });
    }
  
    const [inputText, setInputText] = useState("");
    let inputHandler = (e) => {
      //convert input text to lower case
      var lowerCase = e.target.value.toLowerCase();
      setInputText(lowerCase);
    };
    const filteredData = devis?.filter((el) => {
      if (inputText === '') {
          return el;
      } else {
          return el.email.toLowerCase().includes(inputText)
      }
    })
   if (affiche){
    return (
  <div>
    <div class="app">
    <Sidebar></Sidebar>
  
    <div className="app-wrapper">
      <div className="app-content pt-3 p-md-3 p-lg-4">
        <div className="container-xl">
          <div className="row g-3 mb-4 align-items-center justify-content-between">
            <div className="col-auto">
              <h1 className="app-page-title mb-0">Quotes :</h1>
            </div>
            <div className="col-auto">
              <div className="page-utilities">
                <div className="row g-2 justify-content-start justify-content-md-end align-items-center">
                  <div className="col-auto">
                    <form className="table-search-form row gx-1 align-items-center">
                      <div className="col-auto">
                        <input type="text" id="search-orders" name="search" className="form-control search-orders" placeholder="Search" onChange={inputHandler}/>
                      </div>
                    
                    </form>
                  </div>{/*//col*/}
                 
                </div>{/*//row*/}
              </div>{/*//table-utilities*/}
            </div>{/*//col-auto*/}
          </div>{/*//row*/}
          
          <div className="tab-content" id="orders-table-tab-content">
            <div className="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
              <div className="app-card app-card-orders-table shadow-sm mb-5">
                <div className="app-card-body">
                  <div className="table-responsive">
                    <table className="table app-table-hover mb-0 text-left">
                      <thead>
                        <tr>
                          <th className="cell">email</th>
                          <th className="cell">ServiceId</th>

                          <th className="cell">message</th>
                          <th className='cell'>Action</th>
                         
                        </tr>
                      </thead>
                      <tbody>
                        {filteredData?.map((item)=>{
                          return(
  <tr>                    
                          <td className="cell">{item.email}</td>
                          <td className="cell">{item?.ServiceId}</td>

                          <td className="cell"><span className="truncate">{item.message}</span></td>
                          <td className="cell">
                            

                          {/* <Link to ={`/dashbord/updatedevis/${item.id}`}>
                        <button className="btn btn-default btn-rounded btn-sm"><span className="fa fa-print" /></button>
                        </Link> */}
                             <button className="btn btn-danger btn-rounded btn-sm" onClick={(e)=>supprimer(item.id)}><span className="fa fa-times" /></button>
                      
                          </td>
                         </tr>)
                        })}
                        
                     
                      </tbody>
                    </table>
                  </div>{/*//table-responsive*/}
                </div>{/*//app-card-body*/}		
              </div>{/*//app-card*/}
            
            </div>{/*//tab-pane*/}
          
          </div>{/*//tab-content*/}
        </div>{/*//container-fluid*/}
      </div>{/*//app-content*/}
     
    </div>
  </div></div>
  
  
  
    )
  }}

export default Listdevis