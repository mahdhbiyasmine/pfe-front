import React, { useEffect, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import depotservice from "../../services/depotservice";
import productservice from "../../services/productservice";
import Sidebar from "../sidebar";
function Adddepot() {
  const [data, setData] = useState({});
  const [list, setList] = useState({});
  const [affiche, setAffiche] = useState(false);
  const [errors, setErrors] = useState({});

  const getAllProduct = () => {
    productservice
      .GetAll()
      .then((res) => {
        console.log(res);
        setList(res.data.data);
        setAffiche(true);
      })
      .catch((erreur) => {
        console.log(erreur);
      });
  };
  useEffect(() => {
    getAllProduct();
  }, []);

  const onChangeHandler = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' }); // Clear errors when a field is modified
  };

  const validateName = (name) => {
    return name.trim() !== '';
  };

  const validateAdress = (adress) => {
    return adress.trim() !== '';
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();

    const validationErrors = {};

    if (!data.name || !validateName(data.name)) {
      validationErrors.name = 'Name is required.';
    }

    if (!data.adresse || !validateAdress(data.adresse)) {
      validationErrors.adresse = 'Adress is required.';
    }

    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }

    depotservice
      .create(data)
      .then((res) => {
        console.log(res);
        Navigate('/dashbord/listdepot');
        Swal.fire('Saved!', '', 'success');
      })
      .catch((erreur) => {
        console.log(erreur);
        Swal.fire('Error!', 'Failed to save the depot.', 'error');
      });
  };

  if (affiche) {
    return (
      <div class="app">
        <div class="app-wrapper">
          <div class="app-content pt-3 p-md-3 p-lg-4">
            <div class="container-xl">
              <Sidebar></Sidebar>
              <h1 className="app-page-title">Add depot</h1>
              <hr className="mb-4" />
              <div className="row g-4 settings-section">
                <div className="col-12 col-md-2">
                  <div className="section-intro"></div>{" "}
                </div>
                <div className="col-12 col-md-8">
                  <div className="app-card app-card-settings shadow-sm p-4">
                    <div className="app-card-body">
                      <form className="settings-form">
                        <div className="mb-3">
                          <label htmlFor="setting-input-2" className="form-label">
                            Name
                          </label>

                          <input
                            type="text"
                            className="form-control"
                            id="setting-input-1"
                            placeholder="name"
                            name="name"
                            onChange={onChangeHandler}
                            required
                          />
                          {errors.name && <span className="text-danger">{errors.name}</span>}
                        </div>
                        <div className="mb-3">
                          <label htmlFor="setting-input-2" className="form-label">
                            Adress
                          </label>
                          <textarea
                            type="text"
                            className="form-control"
                            id="setting-input-2"
                            placeholder="adress"
                            required
                            rows={5}
                            name="adresse"
                            defaultValue={""}
                            onChange={onChangeHandler}
                          />
                          {errors.adresse && <span className="text-danger">{errors.adresse}</span>}
                        </div>
                       
                        <button type="submit" className="btn app-btn-primary" onClick={onSubmitHandler}>
                          Save
                        </button>
                      </form>
                    </div>
                    {/*//app-card-body*/}
                  </div>
                  {/*//app-card*/}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Adddepot;
