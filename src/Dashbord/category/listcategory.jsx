import React, { useEffect, useState } from 'react';
import Sidebar from '../sidebar';
import categoryservice from '../../services/categoryservice';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

function Listcategory() {
  const buttonStyle = {
    backgroundColor: '#77dd77', // Blue background
    color: 'white', // White text
    padding: '10px 20px', // Padding
    fontSize: '16px', // Font size
    border: 'none', // No border
    borderRadius: '5px', // Rounded corners
    cursor: 'pointer', // Pointer cursor on hover
    textDecoration: 'none', // Remove underline from Link
    display: 'flex', // Align icon and text
    alignItems: 'center' // Center vertically
  };

  const iconStyle = {
    marginRight: '10px' // Space between icon and text
  };
  const [category, setCategory] = useState([]);
  const [affiche, setAffiche] = useState(false);
  
  const getAllCategory = () => {
    categoryservice.GetAll().then((res) => {
      console.log(res);
      setCategory(res.data.data);
      setAffiche(true);
    }).catch((error) => {
      console.log(error);
    });
  };

  useEffect(() => {
    getAllCategory();
  }, []);

  const supprimer = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        categoryservice.remove(id).then((res) => {
          console.log(res);
          getAllCategory(); // suppression sans refresh
        }).catch((error) => {
          console.log(error);
        });
        Swal.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success"
        });
      }
    });
  };

  const [inputText, setInputText] = useState("");
  const inputHandler = (e) => {
    const lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };

  const filteredData = category?.filter((el) => {
    if (inputText === '') {
      return el;
    } else {
      return el.name.toLowerCase().includes(inputText);
    }
  });

  if (affiche) {
    return (
      <div>
        <div className="app">
          <Sidebar />
          <div className="app-wrapper">
            <div className="app-content pt-3 p-md-3 p-lg-4">
              <div className="container-xl">
                <div className="row g-3 mb-4 align-items-center justify-content-between">
                <div className="col-auto">
      <Link to="/dashbord/addcategory" style={{ textDecoration: 'none' }}>
        <button className="app-page-title mb-0" style={buttonStyle}>
          <span style={iconStyle}>+</span> Add category
        </button>
      </Link>
    </div>
                  <div className="col-auto">
                    <div className="page-utilities">
                      <div className="row g-2 justify-content-start justify-content-md-end align-items-center">
                        <div className="col-auto">
                          <form className="table-search-form row gx-1 align-items-center">
                            <div className="col-auto">
                              <input type="text" id="search-orders" name="search" className="form-control search-orders" placeholder="Search" onChange={inputHandler} />
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="tab-content" id="orders-table-tab-content">
                  <div className="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                    <div className="app-card app-card-orders-table shadow-sm mb-5">
                      <div className="app-card-body">
                        <div className="table-responsive">
                          <table className="table app-table-hover mb-0 text-left">
                            <thead>
                              <tr>
                                <th className="cell">Name</th>
                                <th className="cell">Description</th>
                                <th className="cell">Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              {filteredData?.map((item) => (
                                <tr key={item.id}>
                                  <td className="cell">{item.name}</td>
                                  <td className="cell">{item.description}</td>
                                  <td className="cell">
                                    <Link to={`/dashbord/updatecategory/${item.id}`}>
                                      <button className="btn btn-default btn-rounded btn-sm"><span className="fa fa-pencil" /></button>
                                    </Link>
                                    <button className="btn btn-danger btn-rounded btn-sm" onClick={() => supprimer(item.id)}><span className="fa fa-times" /></button>
                                  </td>
                                </tr>
                              ))}
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  return null;
}

export default Listcategory;
