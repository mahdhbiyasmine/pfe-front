import React, { useEffect, useState } from 'react';
import Sidebar from "../sidebar";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import subcategoryservice from "../../services/subcategoryservice";
import categoryservice from "../../services/categoryservice";

function Addsubcategory() {
  const [data,setData]=useState({})
  const [list,setList]=useState({})
  const [affiche,setAffiche]=useState(false)
  const [errors, setErrors] = useState({});

  const getAllCategory=()=>{
    categoryservice.GetAll().then((res)=>{
         console.log(res)
         setList(res.data.data);
         setAffiche(true)
       }).catch((erreur)=>{
         console.log(erreur)
   
       })
     }
  useEffect(()=>{
    getAllCategory();
  },[])

  const onChangeHandler = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' }); // Clear errors when a field is modified
  };

  const validateName = (name) => {
    // Example validation function for name
    return name.trim() !== '';
  };

  const validateDescription = (description) => {
    // Example validation function for description
    // You can modify this based on your requirements
    return description.trim() !== '';
  };

  const validateCategoryId = (CategoryId) => {
    // Example validation function for CategoryId
    // You can modify this based on your requirements
    return CategoryId !== '';
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();

    const validationErrors = {};

    if (!data.name || !validateName(data.name)) {
      validationErrors.name = 'Name is required.';
    }

    if (!data.description || !validateDescription(data.description)) {
      validationErrors.description = 'Description is required.';
    }

    if (!data.CategoryId || !validateCategoryId(data.CategoryId)) {
      validationErrors.CategoryId = 'Category is required.';
    }

    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }

    // Submit the form if validation passes
    subcategoryservice.create(data)
      .then((res) => {
        console.log(res);
        navigate('/dashbord/subcategory');
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const navigate=useNavigate();

  if (affiche) {
    return (
      <div class="app">
        <div class="app-wrapper">
          <div class="app-content pt-3 p-md-3 p-lg-4">
            <div class="container-xl">
              <Sidebar></Sidebar>
              <h1 className="app-page-title">Add Sub-Category </h1>
              <hr className="mb-4" />
              <div className="row g-4 settings-section">
                <div className="col-12 col-md-2">
                  <div className="section-intro"></div>{" "}
                </div>
                <div className="col-12 col-md-8">
                  <div className="app-card app-card-settings shadow-sm p-4">
                    <div className="app-card-body">
                      <form className="settings-form">
                        <div className="mb-3">
                          <label
                            htmlFor="setting-input-2"
                            className="form-label"
                          >
                            {" "}
                            Name
                          </label>

                          <input
                            type="text"
                            className="form-control"
                            id="setting-input-1"
                            placeholder="name"
                            name="name"
                            onChange={onChangeHandler}
                            required
                          />
                          {errors.name && <span className="text-danger">{errors.name}</span>}
                        </div>
                        <div className="mb-3">
                          <label
                            htmlFor="setting-input-2"
                            className="form-label"
                          >
                            Description
                          </label>
                          <textarea
                            type="text"
                            className="form-control"
                            id="setting-input-2"
                            placeholder="description"
                            required
                            rows={5}
                            name="description" defaultValue={""} onChange={onChangeHandler}
                          />
                          {errors.description && <span className="text-danger">{errors.description}</span>}
                        </div>
                        <div class="mb-3">
                          <select class="form-control" onChange={onChangeHandler} name="CategoryId">
                          <option>Category</option>

                          {list?.map((item)=>{
                  return (
                            <option value={item?.id}>{item?.name}</option>   )
                          })}
                          </select>
                          {errors.CategoryId && <span className="text-danger">{errors.CategoryId}</span>}
                        </div>
                        <button type="submit" className="btn app-btn-primary" onClick={onSubmitHandler} >
                          Save
                        </button>
                      </form>
                    </div>
                    {/*//app-card-body*/}
                  </div>
                  {/*//app-card*/}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Addsubcategory;
