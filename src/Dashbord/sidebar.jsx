import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Sidebar3 from '../user/sidebar3'
import { Button } from 'react-bootstrap'

function Sidebar() {

const navigate = useNavigate()

  const logOut = ()=>{
    localStorage.removeItem('token');
    navigate("/");
  }
  return (
<div>

    <div id="app-sidepanel" className="app-sidepanel" > 
    
      <div id="sidepanel-drop" className="sidepanel-drop" />
      
      <div className="sidepanel-inner d-flex flex-column">
        {/*//app-branding*/}  
        <nav id="app-nav-main" className="app-nav app-nav-main flex-grow-1">
          <ul className="app-menu list-unstyled accordion" id="menu-accordion">
            <li className="nav-item">
              {/*//Bootstrap Icons: https://icons.getbootstrap.com/ */}
              <Link className="nav-link active" to="/dashbord">
                <span className="nav-icon">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-house-door" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M7.646 1.146a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5H9.5a.5.5 0 0 1-.5-.5v-4H7v4a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6zM2.5 7.707V14H6v-4a.5.5 0 0 1 .5-.5h3a.5.5 0 0 1 .5.5v4h3.5V7.707L8 2.207l-5.5 5.5z" />
                    <path fillRule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z" />
                  </svg>
                </span>
                <span className="nav-link-text">Overview</span>
              </Link>{/*//nav-link*/}
            </li>{/*//nav-item*/}
          
            <li className="nav-item has-submenu">
              {/*//Bootstrap Icons: https://icons.getbootstrap.com/ */}
              <a className="nav-link submenu-toggle" href="#" data-bs-toggle="collapse" data-bs-target="#submenu-1" aria-expanded="false" aria-controls="submenu-1">
               
                <span className="nav-link-text">Product</span>
                <span className="submenu-arrow">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                  </svg>
                </span>{/*//submenu-arrow*/}
              </a>{/*//nav-link*/}
              <div id="submenu-1" className="collapse submenu submenu-1" data-bs-parent="#menu-accordion">
                <ul className="submenu-list list-unstyled">
                  <li className="submenu-item"><Link className="submenu-link" to="/dashbord/category"> Category</Link></li>
                  <li className="submenu-item"><Link className="submenu-link" to="/dashbord/subcategory"> SubCategory</Link></li>
                  <li className="submenu-item"><Link className="submenu-link" to="/dashbord/listdepot"> depot</Link></li>
                  <li className="submenu-item"><Link className="submenu-link" to="/dashbord/listprovider"> provider</Link></li>
                  <li className="submenu-item"><Link className="submenu-link" to="/dashbord/product"> Product</Link></li>

                </ul>
              </div>
            </li>{/*//nav-item*/}
            
          
            <li className="nav-item has-submenu">
              {/*//Bootstrap Icons: https://icons.getbootstrap.com/ */}
              <a className="nav-link submenu-toggle" href="#" data-bs-toggle="collapse" data-bs-target="#submenu-4" aria-expanded="false" aria-controls="submenu-4">
               
                <span className="nav-link-text">Service </span>
                <span className="submenu-arrow">
                  <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-chevron-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fillRule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" />
                  </svg>
                </span>{/*//submenu-arrow*/}
              </a>{/*//nav-link*/}
              <div id="submenu-4" className="collapse submenu submenu-4" data-bs-parent="#menu-accordion">
              <ul className="submenu-list list-unstyled">
                  <li className="submenu-item"><Link className="submenu-link" to="/dashbord/listservicecategory"> serviceCategory</Link></li>
                  <li className="submenu-item"><Link className="submenu-link" to="/dashbord/listservice"> service</Link></li>

<li className="submenu-item"><Link className="submenu-link" to="/dashbord/devis"> quotes</Link></li>

                </ul>
              </div>
            </li>{/*//nav-item*/}
       
          </ul>{/*//app-menu*/}
        </nav>{/*//app-nav*/}
        <div className="app-sidepanel-footer">
          <nav className="app-nav app-nav-footer">
            <ul className="app-menu footer-menu list-unstyled">
              <li className="nav-item">
                {/*//Bootstrap Icons: https://icons.getbootstrap.com/ */}
                <Link className="nav-link" to="/dashbord/account">
                <span className="nav-icon">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-file-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v10.755S4 11 8 11s5 1.755 5 1.755V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z" />
                      <path fillRule="evenodd" d="M8 10a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                    </svg>
                  </span>
                  <span className="nav-link-text">compte</span>
                </Link>{/*//nav-link*/}
              </li>{/*//nav-item*/}
              <li className="nav-item">
                {/*//Bootstrap Icons: https://icons.getbootstrap.com/ */}
                <Link className="nav-link" to="/">
                  {/* <span className="nav-icon">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-download" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z" />
                      <path fillRule="evenodd" d="M7.646 11.854a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V1.5a.5.5 0 0 0-1 0v8.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3z" />
                    </svg>
                  </span> */}
                  <span className="nav-link-text">Accueil du site</span>
                </Link>{/*//nav-link*/}
              </li>{/*//nav-item*/}
              <li className="nav-item">
                {/*//Bootstrap Icons: https://icons.getbootstrap.com/ */}
                <div className="nav-link"  >
                  {/* <span className="nav-icon">
                    <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-file-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                      <path fillRule="evenodd" d="M12 1H4a1 1 0 0 0-1 1v10.755S4 11 8 11s5 1.755 5 1.755V2a1 1 0 0 0-1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4z" />
                      <path fillRule="evenodd" d="M8 10a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                    </svg>
                  </span> */}
                  <span className="nav-link-text" onClick={logOut}>Déconnexion</span>
                </div>{/*//nav-link*/}
              </li>{/*//nav-item*/}
            </ul>{/*//footer-menu*/}
          </nav>
        </div>{/*//app-sidepanel-footer*/}
      </div>{/*//sidepanel-inner*/}
    </div>{/*//app-sidepanel*/}
</div>





  )
}

export default Sidebar