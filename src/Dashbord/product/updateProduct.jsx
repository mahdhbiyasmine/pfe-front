import React, { useEffect, useState, } from 'react'
import {useParams,useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import Sidebar from '../sidebar'
import productservice from '../../services/productservice'
import subcategoryservice from '../../services/subcategoryservice'

function UpdateProduct() {
    const [data,setData] =useState({})
    const {id}=useParams()
    const [affiche,setAffiche]=useState(false)
    const[image,SetImage]=useState([]); 



    useEffect(()=>{
        productservice.GetOne(id).then((res)=>{
            console.log(res)
            setData(res.data.data);
        }).catch((erreur)=>{
            console.log(erreur)
        })
    },[])
const onChangeHandler=(e)=>{
    
    setData({...data,[e.target.name]:e.target.value})
    console.log(data)
}
const[list,setList]=useState({})
const getAllSubcategory=()=>{
    subcategoryservice.GetAll().then((res)=>{
      console.log(res)
      setList(res.data.data);
      setAffiche(true)
    }).catch((erreur)=>{
      console.log(erreur)

    })
  }
  useEffect(()=>{
    getAllSubcategory();
  },[])
  const navigate=useNavigate()
  const HandleImage=(e)=>{
    SetImage(e.target.files);
  };
  
const onSubmitHandler=(e)=>{
    e.preventDefault()
    Swal.fire({
        title: "Do you want to save the changes?",
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: "Save",
        denyButtonText: `Don't save`
      }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            //consomation d'API
            productservice.update(id,data).then((res)=>{
                console.log(res)
                //yarja3 lil liste ba3ed update
                navigate('/dashbord/product')
            }).catch((erreur)=>{
                console.log(erreur)
            })
        
          Swal.fire("Saved!", "", "success");
        } 
        else if (result.isDenied) {
          Swal.fire("Changes are not saved", "", "info");
        }
      });
}
if (affiche){
  
    return (
      <div> 
      <div class="app"> 
       
   <div class="app-wrapper">
       
       <div class="app-content pt-3 p-md-3 p-lg-4">
           <div class="container-xl">	
      <Sidebar></Sidebar>
  <h1 className="app-page-title">Update Product :</h1>
  <hr className="mb-4" />
  <div className="row g-4 settings-section">
   <div className="col-12 col-md-2">
     <div className="section-intro"></div> </div>
   <div className="col-12 col-md-8">
     <div className="app-card app-card-settings shadow-sm p-4">
       <div className="app-card-body">
         <form className="settings-form">
        
           <div className="mb-1">
           <label htmlFor="setting-input-2" className="form-label" > Reference</label>
  
             <input type="text" className="form-control" id="setting-input-1" placeholder="reference" name='reference' defaultValue={data?.reference}  required  onChange={onChangeHandler} />
           </div>
           
           <div className="mb-3">
             <label htmlFor="setting-input-2" className="form-label"> Description</label>
             <textarea type="text" className="form-control" id="setting-input-2" placeholder="description" required  rows={5}    defaultValue={data?.description} name='description'  onChange={onChangeHandler} />
           </div>
           <div className="mb-3">
           <label htmlFor="setting-input-2" className="form-label" > Price</label>
  
             <input type="text" className="form-control" id="setting-input-1" placeholder="price" name='price' defaultValue={data?.price}  required onChange={onChangeHandler}  />
           </div>
           <div className="mb-3">
           <label htmlFor="setting-input-2" className="form-label" > Image</label>
  
             <input type="file" className="form-control" id="setting-input-1" placeholder="image" name='image' defaultValue={data?.image}   required onChange={HandleImage} />
           </div>
           <div className="mb-3">
           <label htmlFor="setting-input-2" className="form-label" > Stock</label>
  
             <input type="number" className="form-control" id="setting-input-1" placeholder="stock" name='stock'  defaultValue={data?.stock} required onChange={onChangeHandler} />
           </div> 
           <div className="mb-3">
           <label htmlFor="setting-input-2" className="form-label" > Discount</label>
  
             <input type="number" className="form-control" id="setting-input-1" placeholder="Discount" name='discount' defaultValue={data?.discount}  required onChange={onChangeHandler} />
           </div>
           <div className="mb-3">
           <label htmlFor="setting-input-2" className="form-label" > Quantity</label>
  
             <input type="number" className="form-control" id="setting-input-1" placeholder="quantity" name='quantity' defaultValue={data?.quantity}  required onChange={onChangeHandler} />
           </div>
          
           <div className="mb-3">
           <label htmlFor="setting-input-2" className="form-label" > SubCategory</label>
           <select class="form-control" onChange={onChangeHandler} defaultValue={data?.name} name="SubCategoryId">
                            {
                            list?.map((item)=>{
                    return (
                              <option value={item?.id}>{item?.name}</option>   )
                            })}
                            </select>
           </div> 
  
           
           <button type="submit" className="btn app-btn-primary" onClick={onSubmitHandler} >Save</button>          
  
         </form>
       </div>{/*//app-card-body*/}
     </div>{/*//app-card*/}
   </div>
   </div></div></div>
   </div></div></div>
    )
  }
  }
export default UpdateProduct