import Sidebar from "../sidebar";
import React, { useEffect, useState } from "react";
import { useNavigate,  } from "react-router-dom";
import Swal from "sweetalert2";
import subcategoryservice from "../../services/subcategoryservice";
import productservice from "../../services/productservice";

function AddProduct() {

  const [Data,SetData]=useState({});
  const[image,SetImage]=useState([]); 
  const onChangeHandler=(e)=>{
    SetData({ ...Data,[e.target.name]:e.target.value});
    console.log(Data);};
const navigate=useNavigate();




const onSubmitHandler=(e)=>{
  e.preventDefault();
  const formdata=new FormData();
  formdata.append("reference",Data.reference);
  formdata.append("description",Data.description);
  formdata.append("price",Data.price);
  
  formdata.append("file",Data.image);
  
  formdata.append("stock",Data.stock);
  formdata.append("discount",Data.discount);
console.log(Data)
  Swal.fire({
    title: "Do you want to save the changes?",
    showDenyButton: true,
    showCancelButton: true,
    confirmButtonText: "Save",
    denyButtonText: `Don't save`
  }).then((result) => {
    /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
        //consomation d'API
        productservice.create(formdata).then((res)=>{
            console.log(res)
            //yarja3 lil liste ba3ed update
            navigate('/dashbord/product')
        }).catch((erreur)=>{
            console.log(erreur)
        })
    
      Swal.fire("Saved!", "", "success");
    } 
    else if (result.isDenied) {
      Swal.fire("Changes are not saved", "", "info");
    }
  });
 // const [data,setData] =useState({})


};
const HandleImage=(e)=>{
  SetImage(e.target.files);
};

const[list,setList]=useState({})

const [affiche,setAffiche]=useState(false)

const getAllSubcategory=()=>{
    subcategoryservice.GetAll().then((res)=>{
      console.log(res)
      setList(res.data.data);
      setAffiche(true)
    }).catch((erreur)=>{
      console.log(erreur)

    })
  }
  useEffect(()=>{
    getAllSubcategory();
  },[])
  
    if (affiche) {
  
  return (
    <div> 
    <div class="app"> 
     
 <div class="app-wrapper">
     
     <div class="app-content pt-3 p-md-3 p-lg-4">
         <div class="container-xl">	
    <Sidebar></Sidebar>
<h1 className="app-page-title">Add Product :</h1>
<hr className="mb-4" />
<div className="row g-4 settings-section">
 <div className="col-12 col-md-2">
   <div className="section-intro"></div> </div>
 <div className="col-12 col-md-8">
   <div className="app-card app-card-settings shadow-sm p-4">
     <div className="app-card-body">
       <form className="settings-form">
      
         <div className="mb-1">
         <label htmlFor="setting-input-2" className="form-label" > Reference</label>

           <input type="text" className="form-control" id="setting-input-1" placeholder="reference" name='reference'   required  onChange={onChangeHandler} />
         </div>
         
         <div className="mb-3">
           <label htmlFor="setting-input-2" className="form-label"> Description</label>
           <textarea type="text" className="form-control" id="setting-input-2" placeholder="description" required  rows={5}   name='description'  onChange={onChangeHandler} />
         </div>
         <div className="mb-3">
         <label htmlFor="setting-input-2" className="form-label" > Price</label>

           <input type="text" className="form-control" id="setting-input-1" placeholder="price" name='price'   required onChange={onChangeHandler}  />
         </div>
         <div className="mb-3">
         <label htmlFor="setting-input-2" className="form-label" > Image</label>

           <input type="file" className="form-control" id="setting-input-1" placeholder="image" name='image'   required onChange={HandleImage} />
         </div>
         <div className="mb-3">
         <label htmlFor="setting-input-2" className="form-label" > Stock</label>

           <input type="number" className="form-control" id="setting-input-1" placeholder="stock" name='stock'   required onChange={onChangeHandler} />
         </div> 
         <div className="mb-3">
         <label htmlFor="setting-input-2" className="form-label" > Discount</label>

           <input type="number" className="form-control" id="setting-input-1" placeholder="Discount" name='discount'   required onChange={onChangeHandler} />
         </div>
         <div className="mb-3">
         <label htmlFor="setting-input-2" className="form-label" > Quantity</label>

           <input type="number" className="form-control" id="setting-input-1" placeholder="quantity" name='quantity'   required onChange={onChangeHandler} />
         </div>
        
         <div className="mb-3">
         <label htmlFor="setting-input-2" className="form-label" > SubCategory</label>
         <select class="form-control" onChange={onChangeHandler} name="SubCategoryId">
                          {
                          list?.map((item)=>{
                  return (
                            <option value={item?.id}>{item?.name}</option>   )
                          })}
                          </select>
         </div> 

         
         <button type="submit" className="btn app-btn-primary" onClick={onSubmitHandler} >Save</button>          

       </form>
     </div>{/*//app-card-body*/}
   </div>{/*//app-card*/}
 </div>
 </div></div></div>
 </div></div></div>
  )
}
}
export default AddProduct