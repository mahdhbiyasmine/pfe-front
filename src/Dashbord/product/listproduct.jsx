import React, { useState, useEffect } from 'react';
import Sidebar from '../sidebar';
import { Link } from 'react-router-dom';
import productservice from '../../services/productservice';
import fournisseurservice from '../../services/fournisseurservice';
import depotservice from '../../services/depotservice';
import subcategoryservice from '../../services/subcategoryservice';
import Swal from 'sweetalert2';
import { Modal, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencil, faTimes, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import 'bootstrap/dist/css/bootstrap.min.css';

function Listproduct() {
  const [product, setProduct] = useState([]);
  const [affiche, setAffiche] = useState(false);
  const [inputText, setInputText] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [fournisseurName, setFournisseurName] = useState("");
  const [depotName, setDepotName] = useState("");
  const [subCategoryName, setSubCategoryName] = useState("");

  const getAllProduct = () => {
    productservice.GetAll().then((res) => {
      setProduct(res.data);
      setAffiche(true);
    }).catch((erreur) => { console.log(erreur); });
  };

  useEffect(() => {
    getAllProduct();
  }, []);

  const supprimer = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then((result) => {
      if (result.isConfirmed) {
        productservice.remove(id).then((res) => {
          getAllProduct();
        }).catch((error) => {
          console.log(error);
        });
        Swal.fire({
          title: "Deleted!",
          text: "Your file has been deleted.",
          icon: "success"
        });
      }
    });
  };

  let inputHandler = (e) => {
    var lowerCase = e.target.value.toLowerCase();
    setInputText(lowerCase);
  };

  const filteredData = product?.filter((el) => {
    if (inputText === '') {
      return el;
    } else {
      return el.reference.toLowerCase().includes(inputText);
    }
  });

  const buttonStyle = {
    backgroundColor: '#77dd77',
    color: 'white',
    padding: '10px 20px',
    fontSize: '16px',
    border: 'none',
    borderRadius: '5px',
    cursor: 'pointer',
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center'
  };

  const iconStyle = {
    marginRight: '10px'
  };

  const handleShowModal = (product) => {
    setSelectedProduct(product);
    setShowModal(true);
    fetchFournisseurName(product.FournisseurId);
    fetchDepotName(product.DepotId);
    fetchSubCategoryName(product.SubCategoryId);
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setSelectedProduct(null);
    setFournisseurName("");
    setDepotName("");
    setSubCategoryName("");
  };

  const fetchFournisseurName = (fournisseurId) => {
    fournisseurservice.GetOne(fournisseurId).then((res) => {
      setFournisseurName(res.data.data.email);
    }).catch((error) => {
      console.log(error);
    });
  };

  const fetchDepotName = (depotId) => {
    depotservice.GetOne(depotId).then((res) => {
      setDepotName(res.data.data.name);
    }).catch((error) => {
      console.log(error);
    });
  };

  const fetchSubCategoryName = (subCategoryId) => {
    subcategoryservice.GetOne(subCategoryId).then((res) => {
      setSubCategoryName(res.data.data.name);
    }).catch((error) => {
      console.log(error);
    });
  };

  return (
    <div>
      <Sidebar></Sidebar>
      <div className="app">
        <div className="app-wrapper">
          <div className="app-content pt-3 p-md-3 p-lg-4">
            <div className="container-xl">
              <div className="row g-3 mb-4 align-items-center justify-content-between">
                <div className="col-auto">
                  <Link to="/dashbord/addproduct" style={{ textDecoration: 'none' }}>
                    <button className="app-page-title mb-0" style={buttonStyle}>
                      <span style={iconStyle}>+</span> Add product
                    </button>
                  </Link>
                </div>
                <div className="col-auto">
                  <div className="page-utilities">
                    <div className="row g-2 justify-content-start justify-content-md-end align-items-center">
                      <div className="col-auto">
                        <form className="table-search-form row gx-1 align-items-center">
                          <div className="col-auto">
                            <input type="text" id="search-orders" name="search" className="form-control search-orders" placeholder="Search" onChange={inputHandler} />
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-content" id="orders-table-tab-content">
                <div className="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                  <div className="app-card app-card-orders-table shadow-sm mb-5">
                    <div className="app-card-body">
                      <div className="table-responsive">
                        <table className="table app-table-hover mb-0 text-left">
                          <thead>
                            <tr>
                              <th className="cell">Image</th>
                              <th className="cell">Designation</th>
                              <th className="cell">Price</th>
                              <th className="cell">Quantity</th>
                              <th className="cell">Details</th>
                              <th className="cell">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {filteredData?.map((item) => (
                              <tr key={item.id}>
                                <td className="cell">
                                  <img
                                    style={{ width: '80px', height: '80px' }}
                                    src={`http://localhost:4000/file/product/${item.image}`}
                                    alt={`Product ${item.id}`}
                                  />
                                </td>
                                <td className="cell">{item.reference}</td>
                                <td className="cell">{item.price}</td>
                                <td className="cell">{item.stock}</td>
                                <td className="cell">
                                  <button
                                    className="btn btn-edit btn-rounded btn-sm"
                                    onClick={() => handleShowModal(item)}
                                  >
                                    <FontAwesomeIcon icon={faInfoCircle} />
                                  </button>
                                </td>
                                <td className="cell">
                                  <Link to={`/dashbord/updateproduct/${item.id}`}>
                                    <button className="btn btn-default btn-rounded btn-sm">
                                      <FontAwesomeIcon icon={faPencil} />
                                    </button>
                                  </Link>
                                  <button
                                    className="btn btn-danger btn-rounded btn-sm"
                                    onClick={() => supprimer(item.id)}
                                  >
                                    <FontAwesomeIcon icon={faTimes} />
                                  </button>
                                </td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {selectedProduct && (
                <Modal show={showModal} onHide={handleCloseModal}>
                  <Modal.Header closeButton>
                    <Modal.Title>Product Details</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <img
                      style={{ width: '100%', height: 'auto' }}
                      src={`http://localhost:4000/file/product/${selectedProduct.image}`}
                      alt={`Product ${selectedProduct.id}`}
                    />
                    <p><strong>Designation:</strong> {selectedProduct.reference}</p>
                    <p><strong>Description:</strong> {selectedProduct.description}</p>
                    <p><strong>Subcategory:</strong> {subCategoryName}</p>
                    <p><strong>Price:</strong> {selectedProduct.price}</p>
                    <p><strong>Quantity:</strong> {selectedProduct.stock}</p>
                    <p><strong>Depot:</strong> {depotName}</p>
                    <p><strong>Provider:</strong> {fournisseurName}</p>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>
                      Close
                    </Button>
                  </Modal.Footer>
                </Modal>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Listproduct;
