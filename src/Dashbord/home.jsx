import React from 'react'
import { Outlet } from 'react-router-dom'

import Sidebar from './sidebar'
import Layout from './layout'

function Home() {
  return (
<div class="app">   	
    <Sidebar></Sidebar>
    <div className="app-wrapper">
      <div className="app-content pt-3 p-md-3 p-lg-4" />
      <Outlet></Outlet>
     
    </div>{/*//app-wrapper*/}    					
</div>
  )
}

export default Home