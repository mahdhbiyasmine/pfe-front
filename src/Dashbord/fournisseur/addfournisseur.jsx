import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import fournisseurservice from "../../services/fournisseurservice";
import Sidebar from "../sidebar";

function Addfournisseur() {
  const [phone, setPhone] = useState();
  const [data, setData] = useState({});
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  const onChangeHandler = (value) => {
    setPhone(value);
  };

  const handleInputChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' }); // Clear errors when a field is modified
  };

  const validateEmail = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const validatePhoneNumber = (phoneNumber) => {
    return phoneNumber !== undefined; // You can add more specific validation if needed
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();

    const validationErrors = {};

    if (!data.firstName || data.firstName.trim() === '') {
      validationErrors.firstName = 'First Name is required.';
    }

    if (!data.lastName || data.lastName.trim() === '') {
      validationErrors.lastName = 'Last Name is required.';
    }

    if (!data.email || !validateEmail(data.email)) {
      validationErrors.email = 'Please enter a valid Email.';
    }

    if (!validatePhoneNumber(phone)) {
      validationErrors.phone = 'Phone Number is required.';
    }

    if (!data.address || data.address.trim() === '') {
      validationErrors.address = 'Address is required.';
    }

    if (!data.soldProducts || data.soldProducts.trim() === '') {
      validationErrors.soldProducts = 'Sold Products is required.';
    }

    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }

    // If no validation errors, proceed with form submission
    const formData = { ...data, phone };
    fournisseurservice
      .create(formData)
      .then((res) => {
        console.log(res);
        navigate("/dashbord/listprovider");
        Swal.fire('Saved!', '', 'success');
      })
      .catch((erreur) => {
        console.log(erreur);
      });
  };

  return (
    <div className="app">
      <div className="app-wrapper">
        <div className="app-content pt-3 p-md-3 p-lg-4">
          <div className="container-xl">
            <Sidebar />
            <h1 className="app-page-title">Add Provider </h1>
            <hr className="mb-4" />
            <div className="row g-4 settings-section">
              <div className="col-12 col-md-2">
                <div className="section-intro"></div>{" "}
              </div>
              <div className="col-12 col-md-8">
                <div className="app-card app-card-settings shadow-sm p-4">
                  <div className="app-card-body">
                    <form className="settings-form">
                      <div className="mb-3">
                        <label htmlFor="firstName" className="form-label">
                          First Name
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="firstName"
                          placeholder="First Name"
                          name="firstName"
                          onChange={handleInputChange}
                          required
                        />
                        {errors.firstName && <span className="text-danger">{errors.firstName}</span>}
                      </div>
                      <div className="mb-3">
                        <label htmlFor="lastName" className="form-label">
                          Last Name
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="lastName"
                          placeholder="Last Name"
                          name="lastName"
                          onChange={handleInputChange}
                          required
                        />
                        {errors.lastName && <span className="text-danger">{errors.lastName}</span>}
                      </div>
                      <div className="mb-3">
                        <label htmlFor="email" className="form-label">
                          Email
                        </label>
                        <input
                          type="email"
                          className="form-control"
                          id="email"
                          placeholder="Email"
                          name="email"
                          onChange={handleInputChange}
                          required
                        />
                        {errors.email && <span className="text-danger">{errors.email}</span>}
                      </div>
                      <div className="mb-3">
                        <label htmlFor="phone" className="form-label">
                          Phone
                        </label>
                        <PhoneInput
                          international
                          defaultCountry="TN"
                          value={phone}
                          onChange={onChangeHandler}
                          className="form-control"
                        />
                        {errors.phone && <span className="text-danger">{errors.phone}</span>}
                      </div>
                      <div className="mb-3">
                        <label htmlFor="address" className="form-label">
                          Address
                        </label>
                        <textarea
                          type="text"
                          className="form-control"
                          id="address"
                          placeholder="Address"
                          required
                          rows={5}
                          name="address"
                          onChange={handleInputChange}
                        />
                        {errors.address && <span className="text-danger">{errors.address}</span>}
                      </div>
                      <div className="mb-3">
                        <label htmlFor="soldProducts" className="form-label">
                          Sold Products
                        </label>
                        <input
                          type="text"
                          className="form-control"
                          id="soldProducts"
                          placeholder="Sold Products"
                          name="soldProducts"
                          onChange={handleInputChange}
                          required
                        />
                        {errors.soldProducts && <span className="text-danger">{errors.soldProducts}</span>}
                      </div>
                      <button
                        type="submit"
                        className="btn app-btn-primary"
                        onClick={onSubmitHandler}
                      >
                        Save
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Addfournisseur;
