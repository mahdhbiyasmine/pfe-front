import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { jwtDecode } from "jwt-decode";
import Sidebar2 from "../../dashborduser/sidebar2";
import Sidebar from "../sidebar";

function EditProfile() {
    
  const token = localStorage.getItem("token");
  const decoded = jwtDecode(token);
  const userId = decoded.sub;
  const [dataInfo, setDataInfos] = useState();

  useEffect(() => {
    fetch(`http://localhost:4000/api/users/${userId}`)
      .then((response) => response.json())
      .then((data) => setDataInfos(data));
  }, [userId]);

  
  return (
    <div classname="app">
        <Sidebar></Sidebar>
      <div className="app-wrapper">
        <div className="app-content pt-3 p-md-3 p-lg-4">
          <div className="container-xl">
            <h1 className="app-page-title">My account</h1>
           
      </div>
      {/*//app-wrapper*/}
    </div></div></div>
  );
}

export default EditProfile;
