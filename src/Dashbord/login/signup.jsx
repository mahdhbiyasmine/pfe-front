import loginservice from '../../services/loginservice';
import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import Sidebar3 from '../../user/sidebar3';
import Footer from '../../user/footer';

function Signup() {
  const [data, setData] = useState({});
  const [errors, setErrors] = useState({});
  const navigate = useNavigate();

  const onChangeHandler = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' }); // Effacer les erreurs lorsqu'un champ est modifié
  };
  
  const validateEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const validateLastName = (lastName) => {
    const lastNameRegex = /^[a-zA-Z]+$/;
    return lastNameRegex.test(lastName);
  };
  const validateFirstName = (firstName) => {
    const firstNameRegex = /^[a-zA-Z]+$/;
    return firstNameRegex.test(firstName);
  };

  const onSubmitHandler = async (e) => {
    e.preventDefault();

    const validationErrors = {};

    if (!data.firstName ||!validateFirstName(data.firstName)) {
      validationErrors.firstName = 'First name is required.';
    }

    if (!data.lastName || !validateLastName(data.lastName)) {
      validationErrors.lastName = 'Please enter a valid last name.';
    }

    if (!data.email || !validateEmail(data.email)) {
      validationErrors.email = 'Please enter a valid email address.';
    }

    if (!data.password) {
      validationErrors.password = 'Password is required.';
    }

    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }

    try {
      const userData = { ...data, role: "user" };
      const response = await loginservice.signup(userData);
      
      if (typeof response === 'string') {
        alert('Email already exists!');
      } else {
        alert('User registered successfully!');
        navigate('/login');
      }
    } catch (error) {
      alert('.');
    }
  };

  return (
    <div className="row g-0 app-auth-wrapper">
          <Sidebar3></Sidebar3>

          <div className="app-auth-body mx-auto">  
            <div className="app-auth-branding mb-4" style={{marginTop:"30%",marginRight:'40% '}}></div>
            <h1 style={{marginTop:"5%" ,textAlign:"center",marginLeft:"5%",marginRight:'10% '}}>S'inscrire </h1>               
            <div className="auth-form-container text-start mx-auto">
              <form className="auth-form auth-signup-form" style={{marginTop:"10%"}}>
                <div className="email mb-3">
                  <label className="sr-only" htmlFor="signup-name">First Name</label>
                  <input id="signup-name" name="firstName" type="text" className="single-input"  placeholder="Nom" onChange={onChangeHandler}/>
                  {errors.firstName && <span className="text-danger">{errors.firstName}</span>}
                </div>
                <div className="email mb-3">
                  <label className="sr-only" htmlFor="signup-name">Last Name</label>
                  <input id="signup-lastname" name="lastName" type="text" className="single-input"  placeholder="Prénom" onChange={onChangeHandler} />
                  {errors.lastName && <span className="text-danger">{errors.lastName}</span>}
                </div>
                <div className="email mb-3">
                  <label className="sr-only" htmlFor="signup-email">Email</label>
                  <input id="signup-email" name="email" type="email" className="single-input"  placeholder="Email" onChange={onChangeHandler}/>
                  {errors.email && <span className="text-danger">{errors.email}</span>}
                </div>
                <div className="password mb-3">
                  <label className="sr-only" htmlFor="signup-password">Password</label>
                  <input id="signup-password" name="password" type="password" className="single-input"  placeholder="Mot de passe" onChange={onChangeHandler}/>
                  {errors.password && <span className="text-danger">{errors.password}</span>}
                </div>
                <div className="text-center">
                <button type="submit" className="main_btn" style={{marginTop:"1%",marginLeft:'1%',height:'50px' ,width:'150px'}} onClick={onSubmitHandler}>S'inscrire</button>
                <p style={{marginBottom:'16%',marginTop:'1px',marginLeft:'20%',height:'50px' ,width:'300px'}} className="auth-option text-center pt-5">Already have an account? <Link className="text-link" to ="/login" style={{color:'#41F02D'}}>Connexion</Link></p>

                </div>
              </form>
            </div>
      </div>
      
      <Footer></Footer>
    </div>
  );
}

export default Signup;
