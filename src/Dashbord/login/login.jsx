import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import loginservice from '../../services/loginservice';
import Sidebar3 from '../../user/sidebar3';
import Footer from '../../user/footer';

function Login() {
  const [data, setData] = useState({});
  const [errors, setErrors] = useState({});
  const [error, setError] = useState(null);
  const navigate = useNavigate();

  const onChangeHandler = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
    setErrors({ ...errors, [e.target.name]: '' }); // Clear errors when a field is modified
  };

  const validateEmail = (email) => {
    const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    return emailRegex.test(email);
  };

  const validatePassword = (password) => {
    return password.trim() !== '';
  };

  const onSubmitHandler = (e) => {
    e.preventDefault();

    const validationErrors = {};

    if (!data.email || !validateEmail(data.email)) {
      validationErrors.email = 'Please enter a valid email address.';
    }

    if (!data.password || !validatePassword(data.password)) {
      validationErrors.password = 'Password is required.';
    }

    if (Object.keys(validationErrors).length > 0) {
      setErrors(validationErrors);
      return;
    }
    // loginservice
    // .signIn(data)
    // .then((res) => {
    //   console.log("res",res);
      
    //   localStorage.setItem('token', JSON.stringify(res.data.user.token));
    //   if (res.data.role === 'admin') {
    //     navigate('/dashbord');
    //   } else {
    //     navigate('/userdashboard');
    //   }
    // })
    loginservice
      .signIn(data)
      .then((res) => {
        console.log("res",res.data);
        
        localStorage.setItem('token', JSON.stringify(res.data.access_token
        ));
        console.log("role",res.data.role)
        if (res.data.role === 'admin') {
          navigate('/dashbord');
        } else {
          navigate('/userdashboard');
        }
      })
      .catch((error) => {
        setError('Invalid email or password. Please try again.');
      });
  };

  return (
    <div>
      <Sidebar3 />
      <div class="whole-wrap">
        <div class="container">
          <div className="section-top-border">
            <div className="row">
              <div className="col-lg-8 col-md-8">
                <h3 className="mb-30 title_color" style={{ marginTop: '5%', textAlign: 'center', marginLeft: '15%' }}>
                  Connexion
                </h3>
                <form action="#">
                  <div className="mt-10" style={{ marginTop: '8%', marginLeft: '15%' }}>
                    <input
                      type="email"
                      name="email"
                      placeholder="Email address"
                      required
                      className="single-input"
                      onChange={onChangeHandler}
                    />
                    {errors.email && <div className="text-danger">{errors.email}</div>}
                  </div>
                  <div className="mt-10" style={{ marginTop: '2%', marginLeft: '15%' }}>
                    <input
                      type="password"
                      name="password"
                      placeholder="mot de passe"
                      required
                      className="single-input"
                      onChange={onChangeHandler}
                    />
                    {errors.password && <div className="text-danger">{errors.password}</div>}
                  </div>
                  <div className="mt-10">
                    {error && <div className="text-danger mb-3">{error}</div>}
                      <button
                        type="submit"
                        className="main_btn"
                        style={{ marginTop: '5%', marginLeft: '37%', height: '50px', width: '300px' }}
                        onClick={onSubmitHandler}
                      >
                        Connexion
                      </button>
                    <p style={{ marginTop: '1.5%', marginLeft: '40%', height: '50px', width: '300px' }}>
                      Create an account?{' '}
                      <Link className="text-link" to="/signup" style={{ color: '#41F02D' }}>
                        S'inscrire
                      </Link>
                    </p>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default Login;
