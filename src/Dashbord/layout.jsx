import orderservice from '../services/orderservice'
import { Bar,Line,Doughnut,Radar,PolarArea,Scatter,Pie  } from 'react-chartjs-2'

import React, { useEffect, useState } from 'react'
import productservice from '../services/productservice'
import serviceservice from '../services/serviceservice'
import { Chart, registerables } from 'chart.js'
Chart.register(...registerables);

function Layout() {
  const [order,setOrder]=useState([])
  const getAllorder=()=>{
    orderservice.GetAll().then((res)=>{
      console.log(res)
      setOrder(res.data.data);
    }).catch((erreur)=>{console.log()})
  }
  useEffect(()=>{
    getAllorder()
  },[])
  const nbOrder=order.length;
  //service
      
  const [service,setservice]=useState([])
  const getAllservice=()=>{
    serviceservice.GetAll().then((res)=>{
      console.log(res)
      setservice(res.data.data);
    }).catch((erreur)=>{console.log()})
  }
  useEffect(()=>{
    getAllservice()
  },[])
  const nbService=service.length;
  //Date
  var d=new Date();
var date=d.getFullYear()+" - "+(d.getMonth()+1)+" - "+d.getDate();
console.log(date);
var hours=d.getHours()+ " : "+d.getMinutes();
console.log(hours);



//kkkkkkkk
const pieData = {
    labels: order.map((x) => x.orderDate),
    datasets: [
      {
        label: 'Order Status',
        data: order.map((x) => x.status),
        backgroundColor: [
          'rgba(250, 10, 132, 0.8)',
          'rgba(54, 162, 235, 0.5)',
          'rgba(255, 206, 86, 0.8)',
          'rgba(75, 192, 192, 0.7)',
          'rgba(153, 102, 255, 0.8)',
          'rgba(255, 159, 64, 0.7)',
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
        ],
        borderWidth: 1,
      },
    ],
  };
  return (
<div>
  
  <div className="container-xl">
    <h1 className="app-page-title">  <div className="col-6 col-lg-3" style={{marginLeft:"75%"}}>
        <div className="app-card app-card-stat shadow-sm h-100">
       
          <h4 className="stats-type mb-1" style={{size:"400px"}}>Time :</h4>

            <h4 className="stats-type mb-1">{hours} <br></br>{date}</h4>
          
          <a className="app-card-link-mask" href="#" />
        </div>{/*//app-card*/}
      </div>{/*//col*/} </h1>
           
   
    <div className="row g-4 mb-4">
      <div className="col-6 col-lg-3">
        <div className="app-card app-card-stat shadow-sm h-100">
          <div className="app-card-body p-3 p-lg-4">
            <h4 className="stats-type mb-1">Orders</h4>
            <div className="stats-figure">{nbOrder}</div>
          
          </div>{/*//app-card-body*/}
          <a className="app-card-link-mask" href="#" />
        </div>{/*//app-card*/}
      </div>{/*//col*/}
      <div className="col-6 col-lg-3">
        <div className="app-card app-card-stat shadow-sm h-100">
          <div className="app-card-body p-3 p-lg-4">
            <h4 className="stats-type mb-1">Services</h4>
            <div className="stats-figure">{nbService}</div>
            <div className="stats-meta text-success">
              <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-arrow-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fillRule="evenodd" d="M8 1a.5.5 0 0 1 .5.5v11.793l3.146-3.147a.5.5 0 0 1 .708.708l-4 4a.5.5 0 0 1-.708 0l-4-4a.5.5 0 0 1 .708-.708L7.5 13.293V1.5A.5.5 0 0 1 8 1z" />
              </svg> 5% </div>
          </div>{/*//app-card-body*/}
          <a className="app-card-link-mask" href="#" />
        </div>{/*//app-card*/}
      </div>{/*//col*/}
      <div className="col-6 col-lg-3">
        <div className="app-card app-card-stat shadow-sm h-100">
          <div className="app-card-body p-3 p-lg-4">
            <h4 className="stats-type mb-1">Projects</h4>
            <div className="stats-figure">23</div>
            <div className="stats-meta">
              Open</div>
          </div>{/*//app-card-body*/}
          <a className="app-card-link-mask" href="#" />
        </div>{/*//app-card*/}
      </div>{/*//col*/}
      <div className="col-6 col-lg-3">
        <div className="app-card app-card-stat shadow-sm h-100">
          <div className="app-card-body p-3 p-lg-4">
            <h4 className="stats-type mb-1">Invoices</h4>
            <div className="stats-figure">6</div>
            <div className="stats-meta">New</div>
          </div>{/*//app-card-body*/}
          <a className="app-card-link-mask" href="#" />
        </div>{/*//app-card*/}
      </div>{/*//col*/}
    </div>{/*//row*/}
    <div className="row g-4 mb-4">
      <div className="col-12 col-lg-6">
        <div className="app-card app-card-chart h-100 shadow-sm">
          <div className="app-card-header p-3">
            <div className="row justify-content-between align-items-center">
              <div className="col-auto">
                <h4 className="app-card-title">Order charts:</h4>
              </div>{/*//col*/}
            
              <Pie 
                  data={{
                    labels: order?.map((x) => x.orderDate),
                    datasets: [
                      {
                        label: "# of Votes",
                        data: order?.map((x) => x.status),
                        backgroundColor: [
                          "rgba(250, 10, 132, 0.8)",
                          "rgba(54, 162, 235, 0.5)",
                          "rgba(255, 206, 86, 0.8)",
                          "rgba(75, 192, 192, 0.7)",
                          "rgba(153, 102, 255, 0.8)",
                          "rgba(255, 159, 64, 0.7)",
                        ],
                        borderColor: [
                          "rgba(255, 99, 132, 1)",
                          "rgba(54, 162, 235, 1)",
                          "rgba(255, 206, 86, 1)",
                          "rgba(75, 192, 192, 1)",
                          "rgba(153, 102, 255, 1)",
                          "rgba(255, 159, 64, 1)",
                        ],
                        borderWidth: 1,
                      },
                    ],
                  }}
                  options={{
                    scales: {
                      y: {
                        beginAtZero: true,
                      },
                    },
                  }}
                /> 


            </div>{/*//row*/}
          </div>{/*//app-card-header*/}
          <div className="app-card-body p-3 p-lg-4">
            
            <div className="chart-container">
              <canvas id="canvas-linechart" />
            </div>
          </div>{/*//app-card-body*/}
        </div>{/*//app-card*/}
      </div>{/*//col*/}
      <div className="col-12 col-lg-6">
        <div className="app-card app-card-chart h-100 shadow-sm">
          <div className="app-card-header p-3">
            <div className="row justify-content-between align-items-center">
              <div className="col-auto">
                <h4 className="app-card-title">Orders</h4>
              </div>{/*//col*/}
              <div className="col-auto">
                
              </div>{/*//col*/}
            </div>{/*//row*/}
          </div>{/*//app-card-header*/}
          <div className="app-card-body p-3 p-lg-4">
            <div className="mb-3 d-flex">   
              <select className="form-select form-select-sm ms-auto d-inline-flex w-auto">
                <option value={1} selected>This week</option>
                <option value={2}>Today</option>
                <option value={3}>This Month</option>
                <option value={3}>This Year</option>
              </select>
            </div>
            <div className="chart-container">
              <canvas id="canvas-barchart" />
            </div>
          </div>{/*//app-card-body*/}
        </div>{/*//app-card*/}
      </div>{/*//col*/}
    </div>{/*//row*/}

  </div>		
</div>



  )
}

export default Layout