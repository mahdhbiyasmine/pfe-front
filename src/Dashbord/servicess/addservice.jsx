import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import servicecategoryservice from "../../services/servicecategoryservice";
import serviceservice from "../../services/serviceservice";
import Sidebar from '../sidebar'
function Addservice() {
    const [data, setData] = useState({});
    const [list, setList] = useState({});
    const [affiche, setAffiche] = useState(false);
    const [errors, setErrors] = useState({});

    const getAllServiceCategory = () => {
        servicecategoryservice.GetAll().then((res) => {
            console.log(res);
            setList(res.data.data);
            setAffiche(true);
        }).catch((erreur) => {
            console.log(erreur);
        });
    };

    useEffect(() => {
        getAllServiceCategory();
    }, []);

    const onChangeHandler = (e) => {
        setData({ ...data, [e.target.name]: e.target.value });
        setErrors({ ...errors, [e.target.name]: '' }); // Clear errors when a field is modified
    };

    const validateName = (name) => {
        return name.trim() !== '';
    };

    const validateDescription = (description) => {
        return description.trim() !== '';
    };

    const validateServiceCategoryId = (ServiceCategoryId) => {
        return ServiceCategoryId !== '';
    };

    const validateCost = (cost) => {
        return cost !== undefined && cost !== null && !isNaN(cost) && cost >= 0;
    };

    const onSubmitHandler = (e) => {
        e.preventDefault();

        const validationErrors = {};

        if (!data.name || !validateName(data.name)) {
            validationErrors.name = 'Name is required.';
        }

        if (!data.description || !validateDescription(data.description)) {
            validationErrors.description = 'Description is required.';
        }

        if (!data.ServiceCategoryId || !validateServiceCategoryId(data.ServiceCategoryId)) {
            validationErrors.ServiceCategoryId = 'Service category is required.';
        }

        if (!data.cost || !validateCost(data.cost)) {
            validationErrors.cost = 'Valid cost is required.';
        }

        if (Object.keys(validationErrors).length > 0) {
            setErrors(validationErrors);
            return;
        }

        serviceservice.create(data)
            .then((res) => {
                console.log(res);
                navigate('/dashbord/listservice');
                Swal.fire("Saved!", "", "success");
            })
            .catch((erreur) => {
                console.log(erreur);
                Swal.fire("Error!", "Failed to save the service.", "error");
            });
    };

    const navigate = useNavigate();

    if (affiche) {
        return (
            <div class="app">
                <Sidebar></Sidebar>
                <div class="app-wrapper">
                    <div class="app-content pt-3 p-md-3 p-lg-4">
                        <div class="container-xl">
                            <h1 className="app-page-title">Add Service</h1>
                            <hr className="mb-4" />
                            <div className="row g-4 settings-section">
                                <div className="col-12 col-md-2">
                                    <div className="section-intro"></div>{" "}
                                </div>
                                <div className="col-12 col-md-8">
                                    <div className="app-card app-card-settings shadow-sm p-4">
                                        <div className="app-card-body">
                                            <form className="settings-form">
                                                <div className="mb-3">
                                                    <label htmlFor="setting-input-1" className="form-label">Name</label>
                                                    <input type="text" className="form-control" id="setting-input-1" name="name" onChange={onChangeHandler} required />
                                                    {errors.name && <span className="text-danger">{errors.name}</span>}
                                                </div>
                                                <div className="mb-3">
                                                    <label htmlFor="setting-input-2" className="form-label">Description</label>
                                                    <textarea type="text" className="form-control" id="setting-input-2" name="description" onChange={onChangeHandler} required />
                                                    {errors.description && <span className="text-danger">{errors.description}</span>}
                                                </div>
                                                <div class="mb-3">
                                                    <label htmlFor="service-category" className="form-label">Service Category</label>
                                                    <select class="form-control" id="service-category" onChange={onChangeHandler} name="ServiceCategoryId">
                                                        <option value="">Select Service Category</option>
                                                        {list?.map((item) => (
                                                            <option key={item.id} value={item.id}>{item.name}</option>
                                                        ))}
                                                    </select>
                                                    {errors.ServiceCategoryId && <span className="text-danger">{errors.ServiceCategoryId}</span>}
                                                </div>
                                                <div className="mb-3">
                                                    <label htmlFor="setting-input-3" className="form-label">Cost</label>
                                                    <input type="number" className="form-control" id="setting-input-3" name="cost" onChange={onChangeHandler} required />
                                                    {errors.cost && <span className="text-danger">{errors.cost}</span>}
                                                </div>
                                                <button type="submit" className="btn app-btn-primary" onClick={onSubmitHandler}>Save</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    } else {
        return null;
    }
}

export default Addservice;
