import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import Sidebar from '../sidebar';
import Swal from 'sweetalert2';
import serviceservice from '../../services/serviceservice';
import servicecategory from '../../services/servicecategory';
function Listservice() {
    const [service, setService] = useState([]);
    const [serviceCategories, setServiceCategories] = useState({});
    const [affiche, setAffiche] = useState(false);
    const [inputText, setInputText] = useState("");

    const getAllService = () => {
        serviceservice.GetAll().then((res) => {
            console.log(res);
            setService(res.data.data);
            setAffiche(true);
        }).catch((erreur) => { console.log(erreur); });
    };

    const getServiceCategoryById = (id) => {
        if (serviceCategories[id]) {
            return; // Already fetched
        }
        servicecategory.GetOne(id).then((res) => {
            setServiceCategories((prevCategories) => ({
                ...prevCategories,
                [id]: res.data.data.name
            }));
        }).catch((error) => {
            console.log(error);
        });
    };

    useEffect(() => {
        getAllService();
    }, []);

    useEffect(() => {
        service.forEach((srv) => {
            getServiceCategoryById(srv.ServiceCategoryId);
        });
    }, [service]);

    const supprimer = (id) => {
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then((result) => {
            if (result.isConfirmed) {
                serviceservice.remove(id).then((res) => {
                    console.log(res);
                    getAllService(); // supression sans refresh
                }).catch((error) => {
                    console.log(error);
                });
                Swal.fire({
                    title: "Deleted!",
                    text: "Your file has been deleted.",
                    icon: "success"
                });
            }
        });
    };

    let inputHandler = (e) => {
        var lowerCase = e.target.value.toLowerCase();
        setInputText(lowerCase);
    };

    const filteredData = service?.filter((el) => {
        if (inputText === '') {
            return el;
        } else {
            return el.name.toLowerCase().includes(inputText);
        }
    });

    const buttonStyle = {
        backgroundColor: '#77dd77',
        color: 'white',
        padding: '10px 20px',
        fontSize: '16px',
        border: 'none',
        borderRadius: '5px',
        cursor: 'pointer',
        textDecoration: 'none',
        display: 'flex',
        alignItems: 'center'
    };

    const iconStyle = {
        marginRight: '10px'
    };

    return (
        <div>
            <Sidebar></Sidebar>
            <div className="app">
                <div className="app-wrapper">
                    <div className="app-content pt-3 p-md-3 p-lg-4">
                        <div className="container-xl">
                            <div className="row g-3 mb-4 align-items-center justify-content-between">
                                <div className="col-auto">
                                    <Link to="/dashbord/addservice" style={{ textDecoration: 'none' }}>
                                        <button className="app-page-title mb-0" style={buttonStyle}>
                                            <span style={iconStyle}>+</span> Add service
                                        </button>
                                    </Link>
                                </div>
                                <div className="col-auto">
                                    <div className="page-utilities">
                                        <div className="row g-2 justify-content-start justify-content-md-end align-items-center">
                                            <div className="col-auto">
                                                <form className="table-search-form row gx-1 align-items-center">
                                                    <div className="col-auto">
                                                        <input type="text" id="search-orders" name="search" className="form-control search-orders" placeholder="Search ..." onChange={inputHandler} />
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-content" id="orders-table-tab-content">
                                <div className="tab-pane fade show active" id="orders-all" role="tabpanel" aria-labelledby="orders-all-tab">
                                    <div className="app-card app-card-orders-table shadow-sm mb-5">
                                        <div className="app-card-body">
                                            <div className="table-responsive">
                                                <table className="table app-table-hover mb-0 text-left">
                                                    <thead>
                                                        <tr>
                                                            <th className="cell">Name</th>
                                                            <th className="cell">Description</th>
                                                            <th className='cell'>Service Category</th>
                                                            <th className='cell'>Cost</th>
                                                            <th className="cell">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {filteredData?.map((item) => (
                                                            <tr key={item.id}>
                                                                <td className="cell">{item.name}</td>
                                                                <td className="cell"><span className="truncate">{item.description}</span></td>
                                                                <td className="cell">{serviceCategories[item.ServiceCategoryId]}</td>
                                                                <td className="cell">{item.cost}</td>
                                                                <td className="cell">
                                                                    <Link to={`/dashbord/updateservice/${item.id}`}>
                                                                        <button className="btn btn-default btn-rounded btn-sm"><span className="fa fa-pencil" /></button>
                                                                    </Link>
                                                                    <button className="btn btn-danger btn-rounded btn-sm" onClick={() => supprimer(item.id)}><span className="fa fa-times" /></button>
                                                                </td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Listservice;
