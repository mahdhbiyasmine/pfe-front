import http from"./axiosContext"

const create=(data)=>{
    return http.post(`/Depot`,data)
}
const GetAll=()=>{
    return http.get(`/Depot`)
}
const GetOne=(id)=>{
    return http.get(`/Depot/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/Depot/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/Depot/${id}`);
}
export default{GetAll,create,GetOne,update,remove}