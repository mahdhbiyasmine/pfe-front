import http from"./axiosContext"
const token = localStorage.getItem("token")
const create=(data)=>{
    return http.post(`/CommandLines`,data ,{
        headers: {
            'Authorization': 'Bearer ' + token
          }
    })
}
const GetAll=()=>{
    return http.get(`/CommandLines`)
}
const GetOne=(id)=>{
    return http.get(`/CommandLines/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/CommandLines/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/CommandLines/${id}`);
}
export default{GetAll,create,GetOne,update,remove}