import http from"./axiosContext"

const create=(data)=>{
    return http.post(`/Fournisseur`,data)
}
const GetAll=()=>{
    return http.get(`/Fournisseur`)
}
const GetOne=(id)=>{
    return http.get(`/Fournisseur/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/Fournisseur/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/Fournisseur/${id}`);
}
export default{GetAll,create,GetOne,update,remove}