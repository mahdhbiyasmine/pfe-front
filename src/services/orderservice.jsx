import http from"./axiosContext"

const create=(data)=>{
    return http.post(`/Order`,data)
}
const GetAll=()=>{
    return http.get(`/Order`)
}
const GetOne=(id)=>{
    return http.get(`/Order/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/Order/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/Order/${id}`);
}
export default{GetAll,create,GetOne,update,remove}