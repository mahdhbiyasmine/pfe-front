import http from"./axiosContext"

const create=(data)=>{
    return http.post(`/Service`,data)
}
const GetAll=()=>{
    return http.get(`/Service`)
}
const GetOne=(id)=>{
    return http.get(`/Service/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/Service/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/Service/${id}`);
}
export default{GetAll,create,GetOne,update,remove}