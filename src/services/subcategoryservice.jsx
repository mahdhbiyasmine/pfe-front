import http from"./axiosContext"

const create=(data)=>{
    return http.post(`/SubCategory`,data)
}
const GetAll=()=>{
    return http.get(`/SubCategory`)
}
const GetOne=(id)=>{
    return http.get(`/SubCategory/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/SubCategory/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/SubCategory/${id}`);
}
export default{GetAll,create,GetOne,update,remove}