import http from"./axiosContext"

const create=(data)=>{
    return http.post(`/Quote`,data)
}
const GetAll=()=>{
    return http.get(`/Quote`)
}
const GetOne=(id)=>{
    return http.get(`/Quote/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/Quote/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/Quote/${id}`);
}
export default{GetAll,create,GetOne,update,remove}