import http from"./axiosContext"
const token = localStorage.getItem("token")
const create=(data)=>{
    return http.post(`/category`,data ,{
        headers: {
            'Authorization': 'Bearer ' + token
          }
    })
}
// const create=(data)=>{
//     return http.post(`/category`,data)
// }
const GetAll=()=>{
    return http.get(`/category`)
}
const GetOne=(id)=>{
    return http.get(`/category/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/category/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/category/${id}`);
}
export default{GetAll,create,GetOne,update,remove}