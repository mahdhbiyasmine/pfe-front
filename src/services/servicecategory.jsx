import http from"./axiosContext"

const create=(data)=>{
    return http.post(`/ServiceCategory`,data)
}
const GetAll=()=>{
    return http.get(`/ServiceCategory`)
}
const GetOne=(id)=>{
    return http.get(`/ServiceCategory/${id}`);
}
const update=(id,Data)=>{
   
        return http.put(`/ServiceCategory/${id}`,Data)
}
const remove =(id)=>{
    return http.delete(`/ServiceCategory/${id}`);
}
export default{GetAll,create,GetOne,update,remove}