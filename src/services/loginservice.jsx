import http from"./axiosContext"

const signup=(data)=>{
    return http.post(`/api/auth/signup`,data)
}
const signIn=(data)=>{
    return http.post(`/api/auth/login`,data)
}
const GetAll=()=>{
    return http.get(`/api/users`);
}

export default{signup,signIn,GetAll}